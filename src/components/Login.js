import React, {useRef, useState} from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import BlackSpiritEfficiency from "../assets/images/icons/BlackSpiritEfficiency.png";

import AuthService from "../services/auth.service";

const required = (value) => {
    if (!value) {
        return (
            <div className="text-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

/**
 * Login page
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
const Login = (props) => {
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    };

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };

    const handleLogin = (e) => {
        e.preventDefault();

        setMessage("");
        setLoading(true);

        form.current.validateAll();

        if (checkBtn.current.context._errors.length === 0) {
            AuthService.login(username, password).then(
                () => {
                    props.history.push("/profile");
                    window.location.reload();
                },
                (error) => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setLoading(false);
                    setMessage(resMessage);
                }
            );
        } else {
            setLoading(false);
        }
    };

    return (
        <div className="block login-container">
            <img
                src={BlackSpiritEfficiency}
                alt="profile-img"
                className="profile-logo"
            />
            {message && (
                <div className="alert alert-danger bg-danger login-alert" role="alert">
                    Bad Credentials
                </div>
            )}
            <Form onSubmit={handleLogin} ref={form}>
                <div>
                    <label htmlFor="username">Username</label>
                    <Input
                        type="text"
                        className="input-outlined"
                        name="username"
                        value={username}
                        onChange={onChangeUsername}
                        validations={[required]}
                    />
                </div>

                <div>
                    <label htmlFor="password">Password</label>
                    <Input
                        type="password"
                        className="input-outlined"
                        name="password"
                        value={password}
                        onChange={onChangePassword}
                        validations={[required]}
                    />
                </div>

                <div className={"btn-login-container"}>
                    <button className="btn-login" disabled={loading}>
                        {loading && (
                            <span className="spinner-border spinner-border-sm"/>
                        )}
                        <span>Login</span>
                    </button>
                </div>

                <CheckButton style={{display: "none"}} ref={checkBtn}/>
            </Form>
        </div>
    );
};

export default Login;
