import React from 'react'
import {Modal, Button} from "react-bootstrap";

/**
 * Modal Shown on Delete Confirmation
 *
 * @param showModal true to show modal
 * @param hideModal true to hide modal
 * @param confirmModal callback on modal confirmation
 * @param id unique identifier of the object to delete
 * @param type type of the object to delete
 * @param message message to display
 * @returns {JSX.Element}
 * @constructor
 */
const DeleteConfirmation = ({showModal, hideModal, confirmModal, id, type, message}) => {
    return (
        <Modal show={showModal} onHide={hideModal}>
            <Modal.Header closeButton>
                <Modal.Title>Delete Confirmation</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="block text-danger">{message}</div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="default" onClick={hideModal}>
                    Cancel
                </Button>
                <Button variant="danger" onClick={() => confirmModal(type, id)}>
                    Delete
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default DeleteConfirmation;
