import React, {useState} from "react";
import AuthService from "../services/auth.service";
import {Redirect} from "react-router-dom";

/**
 * Home Page
 * @returns {JSX.Element}
 * @constructor
 */
const Home = () => {
    const currentUser = AuthService.getCurrentUser();
    const [signupRedirect, setSignupRedirect] = useState(false);
    return (
        <div>

            {/* Redirect to Signup */}
            {signupRedirect && (
                <div>
                    <Redirect to={{pathname: '/register'}}/>
                </div>
            )}

            <header className="page-header">
                <h1>Welcome to BDO&nbsp;Efficiency&nbsp;!</h1>
            </header>

            <div>
                <div className={"block block-home"}>
                    <h4>
                        Calculators <i className="fas fa-cogs"/>
                    </h4>
                    <p>
                        BDO Efficiency got you covered with tools for Black Desert Online, such as profit
                        calculators for Memory Fragment scrolls, Corrupted Magic Crystals, a Caphras stone-level a
                        Marketplace Tax calculator and many
                        more others to come.
                    </p>
                    <p>
                        You will also be provided with tools to create characters and save your grind sessions and see
                        your
                        average money per hour per grindspots.
                    </p>

                    <h4 className={"graphics-header"}>Graphics <i className="fas fa-chart-bar"/></h4>
                    <p>
                        BDO Efficiency will use the data of your characters and grind sessions to display various
                        statistics
                        about the classes and money gained per hour on the different grindspots.
                    </p>
                    <p>
                        This will allow you to compare your grinding results with others of the same class and gear.
                    </p>

                    {/* CALL TO ACTION IF NOT CONNECTED */}
                    {currentUser == null && (
                        <div className={"text-center"}>
                            <button className="begin-adventure" onClick={() => setSignupRedirect(true)}>
                                Begin your adventure
                            </button>
                        </div>
                    )}
                </div>
            </div>

        </div>
    );
};

export default Home;
