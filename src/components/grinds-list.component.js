import React, {Component} from "react";
import GrindDataService from "../services/grind.service";
import "bootstrap-icons/font/bootstrap-icons.css";
import {Link, Redirect} from "react-router-dom";
import UserService from "../services/user.service";

/**
 * List of all grinds for admin
 */
export default class GrindsList extends Component {
    constructor(props) {
        super(props);
        this.retrieveGrinds = this.retrieveGrinds.bind(this);
        this.delete = this.delete.bind(this);
        this.setGrindRedirect = this.setGrindRedirect.bind(this);
        this.checkAuth = this.checkAuth.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);

        this.state = {
            grinds: [],
            currentItem: null,
            currentIndex: -1,
            searchName: "",
            grindRedirect: "",
            authorized: true,
        };
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getAdminBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    componentDidMount() {

        // Check Auth
        this.checkAuth();

        this.retrieveGrinds();
    }

    /**
     * Retrieve Grind sessions
     */
    retrieveGrinds() {
        // Retrieve all grinds
        GrindDataService.getAll()
            .then(response => {
                this.setState({
                    grinds: response.data,
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    setGrindRedirect(path) {
        this.setState({
            grindRedirect: path
        });
    }

    /**
     * Delete a Grind session
     *
     * @param grindId Unique identifier of the grind session
     */
    delete(grindId) {
        GrindDataService.delete(grindId)
            .then(response => {
                // Once we receive a successfull response we refresh the grinds list
                this.retrieveGrinds();
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        return (
            <div className="block no-background">
                {/* Redirect to unauthorized page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}

                <div>
                    <h4>Grinds List</h4>
                    <div className={"table-scroll"}>
                        <table className={"table table-bordered table-striped table-dark table-hover-purple"}>
                            <thead className={"thead-colored"}>
                            <tr>
                                <th>User</th>
                                <th>Grindspot</th>
                                <th>Character</th>
                                <th>Class</th>
                                <th>AP</th>
                                <th>AAP</th>
                                <th>DP</th>
                                <th>Off-hand</th>
                                <th>Duration</th>
                                <th>Silver</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            {this.state.grinds &&
                            this.state.grinds.map((grindSession) => (
                                <tr key={grindSession.id}>
                                    <td>{grindSession.user.username}</td>
                                    <td>{grindSession.grindspot.name}</td>
                                    <td>{grindSession.c_character.name}</td>
                                    <td>{grindSession.c_character.classe.name}</td>
                                    <td>{grindSession.c_character.ap}</td>
                                    <td>{grindSession.c_character.aap}</td>
                                    <td>{grindSession.c_character.dp}</td>
                                    {/* CONDITIONAL RENDERING ON KUTUM*/}
                                    {grindSession.c_character.kutum === 0 &&
                                    <td>Nouver</td>
                                    }
                                    {grindSession.c_character.kutum === 1 &&
                                    <td>TET Kutum</td>
                                    }
                                    {grindSession.c_character.kutum === 2 &&
                                    <td>PEN Kutum</td>
                                    }
                                    <td>{Math.floor(grindSession.duration / (1000 * 60 * 60))}
                                        h
                                        {Math.floor(grindSession.duration / (1000 * 60)) % 60}
                                        m
                                        {Math.floor(grindSession.duration / 1000) % 60}
                                        s
                                    </td>
                                    <td>{grindSession.totalSilver.toLocaleString()}</td>
                                    <td className={"actions-cells-admin"}>
                                        {/* REDIRECT TO SHOW */}
                                        <Link
                                            to={{
                                                pathname: "/grinds/show/" + grindSession.id,
                                            }}
                                        >
                                            <i className="bi bi-eye-fill"/>
                                        </Link>

                                        {/* REDIRECT TO EDIT */}
                                        <Link
                                            to={{
                                                pathname: "/grinds/edit/" + grindSession.id,
                                            }}
                                        >
                                            <i className="bi bi-pencil-fill text-warning"/>
                                        </Link>


                                        {/* Icon used as a button to remove the items */}
                                        <i className="fas fa-trash-alt red-text"
                                           onClick={() => this.delete(grindSession.id)}/>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}
