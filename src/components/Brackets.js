import React from "react";

/**
 * AP - DP Brackets
 * @returns {JSX.Element}
 * @constructor
 */
const Brackets = () => {

    return (
        <div className={"page-brackets"}>

            <header className="page-header">
                <h1>AP & DP Brackets</h1>
            </header>

            <div className={"block grid-container-2"}>
                <div className={"col1"}>
                    <h4>AP</h4>
                    <table className={"table table-bordered table-striped table-dark table-hover-purple"}>
                        <thead className={"thead-colored"}>
                        <tr>
                            <th>AP</th>
                            <th>Bonus AP</th>
                            <th>Increase</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>100 - 139</td>
                            <td>5</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>140 - 169</td>
                            <td>10</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>170 - 183</td>
                            <td>15</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>184 - 208</td>
                            <td>20</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>209 - 234</td>
                            <td>30</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td>235 - 244</td>
                            <td>40</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td>245 - 248</td>
                            <td>48</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <td>249 - 252</td>
                            <td>57</td>
                            <td>9</td>
                        </tr>
                        <tr>
                            <td>253 - 256</td>
                            <td>69</td>
                            <td>12</td>
                        </tr>
                        <tr>
                            <td>257 - 260</td>
                            <td>83</td>
                            <td>14</td>
                        </tr>
                        <tr>
                            <td>261 - 264</td>
                            <td>101</td>
                            <td>18</td>
                        </tr>
                        <tr>
                            <td>265 - 268</td>
                            <td>122</td>
                            <td>21</td>
                        </tr>
                        <tr>
                            <td>269 - 272</td>
                            <td>137</td>
                            <td>15</td>
                        </tr>
                        <tr>
                            <td>273 - 276</td>
                            <td>142</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>277 - 280</td>
                            <td>148</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>281 - 284</td>
                            <td>156</td>
                            <td>6</td>
                        </tr>
                        <tr>
                            <td>285 - 288</td>
                            <td>160</td>
                            <td>6</td>
                        </tr>
                        <tr>
                            <td>289 - 292</td>
                            <td>167</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>293 - 296</td>
                            <td>174</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>297 - 300</td>
                            <td>181</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>301 - 304</td>
                            <td>188</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>305 - 308</td>
                            <td>196</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <td>309 - 315</td>
                            <td>200</td>
                            <td>4</td>
                        </tr>
                        <tr>
                            <td>316 - 322</td>
                            <td>203</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>323 - 329</td>
                            <td>205</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>330 - 339</td>
                            <td>207</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>340 +</td>
                            <td>210</td>
                            <td>3</td>
                        </tr>
                        </tbody>
                    </table>
                </div>


                <div className={"col1"}>
                    <h4>DP</h4>
                    <table className={"table table-bordered table-striped table-dark table-hover-purple"}>
                        <thead className={"thead-colored"}>
                        <tr>
                            <th>DP</th>
                            <th>Bonus DR</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>203 - 210</td>
                            <td>1 %</td>
                        </tr>
                        <tr>
                            <td>211 - 217</td>
                            <td>2 %</td>
                        </tr>
                        <tr>
                            <td>218 - 225</td>
                            <td>3 %</td>
                        </tr>
                        <tr>
                            <td>226 - 232</td>
                            <td>4 %</td>
                        </tr>
                        <tr>
                            <td>233 - 240</td>
                            <td>5 %</td>
                        </tr>
                        <tr>
                            <td>241 - 247</td>
                            <td>6 %</td>
                        </tr>
                        <tr>
                            <td>248 - 255</td>
                            <td>7 %</td>
                        </tr>
                        <tr>
                            <td>256 - 262</td>
                            <td>8 %</td>
                        </tr>
                        <tr>
                            <td>263 - 270</td>
                            <td>9 %</td>
                        </tr>
                        <tr>
                            <td>271 - 277</td>
                            <td>10 %</td>
                        </tr>
                        <tr>
                            <td>278 - 285</td>
                            <td>11 %</td>
                        </tr>
                        <tr>
                            <td>286 - 292</td>
                            <td>12 %</td>
                        </tr>
                        <tr>
                            <td>293 - 300</td>
                            <td>13 %</td>
                        </tr>
                        <tr>
                            <td>301 - 307</td>
                            <td>14 %</td>
                        </tr>
                        <tr>
                            <td>308 - 314</td>
                            <td>15 %</td>
                        </tr>
                        <tr>
                            <td>315 - 321</td>
                            <td>16 %</td>
                        </tr>
                        <tr>
                            <td>322 - 328</td>
                            <td>17 %</td>
                        </tr>
                        <tr>
                            <td>329 - 334</td>
                            <td>18 %</td>
                        </tr>
                        <tr>
                            <td>335 - 340</td>
                            <td>19 %</td>
                        </tr>
                        <tr>
                            <td>341 - 346</td>
                            <td>20 %</td>
                        </tr>
                        <tr>
                            <td>347 - 352</td>
                            <td>21 %</td>
                        </tr>
                        <tr>
                            <td>353 - 358</td>
                            <td>22 %</td>
                        </tr>
                        <tr>
                            <td>359 - 364</td>
                            <td>23 %</td>
                        </tr>
                        <tr>
                            <td>365 - 370</td>
                            <td>24 %</td>
                        </tr>
                        <tr>
                            <td>371 - 376</td>
                            <td>25 %</td>
                        </tr>
                        <tr>
                            <td>377 - 382</td>
                            <td>26 %</td>
                        </tr>
                        <tr>
                            <td>383 - 388</td>
                            <td>27 %</td>
                        </tr>
                        <tr>
                            <td>389 - 394</td>
                            <td>28 %</td>
                        </tr>
                        <tr>
                            <td>395 - 400</td>
                            <td>29 %</td>
                        </tr>
                        <tr>
                            <td>401+</td>
                            <td>30 %</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default Brackets;
