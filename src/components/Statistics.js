import React, {useEffect, useState} from "react";
import AuthService from "../services/auth.service";
import GrindDataService from "../services/grind.service";
import ClasseDataService from "../services/classe.service";
import GrindspotDataService from "../services/grindspot.service";
import {Bar, Pie} from 'react-chartjs-2';
import {Redirect} from "react-router-dom";

/**
 * Component used to display statistics between players
 *
 * @returns {JSX.Element}
 * @constructor
 */
const Statistics = () => {
    const [averagePerHourByClass, setAveragePerHourByClass] = useState({});
    const [classPopulation, setClassPopulation] = useState({});
    const [grindspotPopulation, setGrindspotPopulation] = useState({});
    const [grindSessions, setGrindSessions] = useState([]);
    const [createRedirect, setCreateRedirect] = useState(false);
    const [authorized, setAuthorized] = useState(true);

    const optionsAveragePerHoursByClass = {
        scaleShowValues: true,
        plugins: {
            legend: {
                display: false
            }
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
            xAxes: [
                {
                    ticks: {
                        autoSkip: false,
                    },
                },
            ],
        },
    };
    const optionsClassPopulation = {
        scaleShowValues: true,
        plugins: {
            legend: {
                display: false
            }
        },
    };

    useEffect(() => {
        // Check auth
        let user = AuthService.getCurrentUser();

        if (user) {
            setAuthorized(true);
        } else {
            setAuthorized(false);
        }


        // Retrieve infos for Average Money per hour per class
        GrindDataService.getAveragePerHourByClass()
            .then(response => {
                let classes = [];
                let data = [];

                for (let value of Object.values(response.data)) {
                    classes.push(value.classe.name);
                    data.push(value.average);
                }

                setAveragePerHourByClass({
                    labels: classes,
                    datasets: [
                        {
                            label: 'Average Money per Hour',
                            data: data,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                            ],
                            borderWidth: 1,
                        },
                    ],
                });
            })
            .catch(e => {
                console.log(e);
            });

        // Retrieve infos for Class Population
        ClasseDataService.getAllCount()
            .then(response => {

                let classes = [];
                let data = [];

                for (let value of Object.values(response.data)) {
                    classes.push(value.classe.name);
                    data.push(value.count);
                }

                setClassPopulation({
                    labels: classes,
                    datasets: [
                        {
                            label: '',
                            data: data,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                            ],
                            borderWidth: 1,
                        },
                    ],
                });
            })
            .catch(e => {
                console.log(e);
            });

        // Retrieve infos for Class Population
        GrindspotDataService.getAllCount()
            .then(response => {

                let grindspots = [];
                let data = [];

                for (let value of Object.values(response.data)) {
                    grindspots.push(value.grindspot.name);
                    data.push(value.count);
                }

                setGrindspotPopulation({
                    labels: grindspots,
                    datasets: [
                        {
                            label: '',
                            data: data,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                            ],
                            borderWidth: 1,
                        },
                    ],
                });
            })
            .catch(e => {
                console.log(e);
            });


        // Retrieve a limited number of grind sessions
        GrindDataService.getLimitedNumberOfGrinds(true)
            .then(response => {
                setGrindSessions(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }, []);

    return (
        <div className={"page-statistics"}>
            {/* Redirect to add Grind component */}
            {createRedirect && (
                <div>
                    <Redirect to={{pathname: '/addGrind'}}/>
                </div>
            )}

            {!authorized && (
                <div>
                    <Redirect to={{pathname: '/unauthorized'}}/>
                </div>
            )}

            <header className="page-header">
                <h1>Statistics</h1>
            </header>

            <div className="block grid-container-2">
                <div className="col1">
                    <h4>Class Popularity</h4>
                    <Pie data={classPopulation} options={optionsClassPopulation}/>
                </div>
                <div className="col1">
                    <h4>Grindspot Popularity</h4>
                    <Pie data={grindspotPopulation} options={optionsClassPopulation}/>
                </div>
            </div>

            <div className={"block"}>
                <h4>Average Money per hour per class</h4>
                <Bar data={averagePerHourByClass} options={optionsAveragePerHoursByClass}/>
            </div>

            <div className={"block no-background"}>
                <h4>Last 10 Grind Sessions</h4>

                {/*  Lists of grind session */}
                <div className={"table-scroll"}>
                    <table className={"table table-bordered table-striped table-dark table-hover-purple"}>
                        <thead className={"thead-colored"}>
                        <tr>
                            <th>Grindspot</th>
                            <th>Character</th>
                            <th>Class</th>
                            <th>AP</th>
                            <th>AAP</th>
                            <th>DP</th>
                            <th>Off-hand</th>
                            <th>Duration</th>
                            <th>Silver</th>
                        </tr>
                        </thead>
                        <tbody>
                        {grindSessions &&
                        grindSessions.map((grindSession, index) => (
                            <tr key={grindSession.id}>
                                <td>{grindSession.grindspot.name}</td>
                                <td>{grindSession.c_character.name}</td>
                                <td>{grindSession.c_character.classe.name}</td>
                                <td>{grindSession.c_character.ap}</td>
                                <td>{grindSession.c_character.aap}</td>
                                <td>{grindSession.c_character.dp}</td>

                                {/* CONDITIONAL RENDERING ON KUTUM*/}
                                {grindSession.c_character.kutum === 0 &&
                                <td>Nouver</td>
                                }
                                {grindSession.c_character.kutum === 1 &&
                                <td>TET Kutum</td>
                                }
                                {grindSession.c_character.kutum === 2 &&
                                <td>PEN Kutum</td>
                                }

                                <td>{Math.floor(grindSession.duration / (1000 * 60 * 60))}
                                    h
                                    {Math.floor(grindSession.duration / (1000 * 60)) % 60}
                                    m
                                    {Math.floor(grindSession.duration / 1000) % 60}
                                    s
                                </td>
                                <td>{grindSession.totalSilver.toLocaleString()}</td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
                <button className={"button-circle"} onClick={() => setCreateRedirect(true)}><i
                    className="bi bi-plus-lg"/>
                </button>
            </div>
        </div>
    );
};

export default Statistics;
