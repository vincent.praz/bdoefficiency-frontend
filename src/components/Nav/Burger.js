import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import {NavLink} from "react-router-dom";
import AuthService from "../../services/auth.service";

/**
 * Component for the Burger Menu
 *
 * @returns {JSX.Element}
 * @constructor
 */
const Burger = () => {
    const [open, setOpen] = useState(false)
    const [currentUser, setCurrentUser] = useState(undefined);
    const mediaQuery = window.matchMedia('(max-width: 768px)');

    useEffect(() => {
            // Avoid blocking the scroll when the burger menu isn't displayed
            if (open && mediaQuery.matches) {
                document.body.style.overflow = 'hidden'
            } else {
                document.body.style.overflow = 'unset'
            }
        },
        [open]);

    useEffect(() => {
        const user = AuthService.getCurrentUser();

        if (user) {
            setCurrentUser(user);
        }
    }, []);

    return (
        <>
            <StyledBurger open={open} onClick={() => setOpen(!open)}>
                <div/>
                <div/>
                <div/>
            </StyledBurger>
            <Content open={open} className="navbar-nav mr-auto" onClick={() => setOpen(!open)}>
                <li className="nav-item">
                    <NavLink to={"/home"} className="nav-link nav-logo"/>
                </li>

                {currentUser && (
                    <li className="nav-item">
                        <NavLink to={"/statistics"} className="nav-link">
                            Statistics
                        </NavLink>
                    </li>
                )}


                {currentUser && (
                    <>
                        <li className="nav-item">
                            <NavLink to={"/corrupted"} className="nav-link">
                                Corrupted
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to={"/scroll"} className="nav-link">
                                Scrolls
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to={"/caphras"} className="nav-link">
                                Caphras
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to={"/tax"} className="nav-link">
                                Tax Calculator
                            </NavLink>
                        </li>

                        <li className="nav-item">
                            <NavLink to={"/brackets"} className="nav-link">
                                AP - DP Brackets
                            </NavLink>
                        </li>
                    </>
                )}
            </Content>
        </>
    )
};

const StyledBurger = styled.div`
  width: 2rem;
  height: 2rem;
  z-index: 20;
  display: none;
  cursor: pointer;
  @media (max-width: 768px) {
    display: flex;
    justify-content: space-around;
    flex-flow: column nowrap;
  }

  div {
    width: 2rem;
    height: 0.25rem;
    background-color: ${({open}) => open ? '#fff' : '#fff'};
    border-radius: 10px;
    transform-origin: 1px;
    transition: all 0.3s linear;

    &:nth-child(1) {
      transform: ${({open}) => open ? 'rotate(45deg)' : 'rotate(0)'};
    }

    &:nth-child(2) {
      transform: ${({open}) => open ? 'translateX(100%)' : 'translateX(0)'};
      opacity: ${({open}) => open ? 0 : 1};
    }

    &:nth-child(3) {
      transform: ${({open}) => open ? 'rotate(-45deg)' : 'rotate(0)'};
    }
  }
`;

const Content = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;

  &.navbar-nav {
    @media (max-width: 767.98px) {
      justify-content: center;
      flex-direction: column;
      align-items: center;
    }
  }

  @media (max-width: 767.98px) {
    flex-flow: column nowrap;
    background-color: #252533;
    position: fixed;
    z-index: 10;
    transform: ${({open}) => open ? 'translateX(0)' : 'translateX(100%)'};
    top: 0;
    right: 0;
    height: 100vh;
    width: 100%;
    transition: transform 0.3s ease-in-out;
    li {
      color: #fff;
    }
  }
`;

export default Burger;
