import {NavLink} from "react-router-dom";
import React, {useEffect, useState} from "react";
import Burger from './Burger';
import AuthService from "../../services/auth.service";
import ItemDataService from "../../services/item.service";

/**
 * Component for Navbar
 *
 * @returns {JSX.Element}
 * @constructor
 */
const Navbar = () => {
    const [showModeratorBoard, setShowModeratorBoard] = useState(false);
    const [showAdminBoard, setShowAdminBoard] = useState(false);
    const [currentUser, setCurrentUser] = useState(undefined);

    useEffect(() => {
        const user = AuthService.getCurrentUser();

        if (user) {
            setCurrentUser(user);
            setShowModeratorBoard(user.roles.includes("ROLE_MODERATOR"));
            setShowAdminBoard(user.roles.includes("ROLE_ADMIN"));
        }
    }, []);


    const logOut = () => {
        AuthService.logout();
    };

    const refreshPrices = () => {
        // Recalculate all prices
        ItemDataService.recalculateAllPrices()
            .then(() =>
                // Reload page when prices are recalculated
                window.location.reload(false)
            );
    };
    return (
        <nav className="navbar navbar-expand">
            <Burger/>
            {currentUser ? (
                <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <a href={"#"} className="nav-link refresh-price" onClick={refreshPrices}>
                            <i className="bi bi-arrow-repeat"/>
                            <span className="mobile-hidden">Refresh Prices</span>
                        </a>
                    </li>

                    {showModeratorBoard && (
                        <li className="nav-item">
                            <NavLink to={"/mod"} className="nav-link">
                                Moderator Board
                            </NavLink>
                        </li>
                    )}

                    {showAdminBoard && (
                        <li className="nav-item">
                            <NavLink to={"/admin"} className="nav-link">
                                <i className="bi bi-person-lines-fill"/>
                                <span className="mobile-hidden">Admin Board</span>
                            </NavLink>
                        </li>
                    )}


                    <li className="nav-item">
                        <NavLink to={"/profile"} className="nav-link">
                            <i className="fas fa-user"/>
                            <span className="mobile-hidden">{currentUser.username}</span>
                        </NavLink>

                    </li>

                    <li className="nav-item">
                        <a href="/login" className="nav-link" onClick={logOut}>
                            <i className="fa fa-power-off" aria-hidden="true"/>
                        </a>
                    </li>
                </div>
            ) : (
                <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <NavLink to={"/login"} className="nav-link">
                            Login
                        </NavLink>
                    </li>

                    <li className="nav-item">
                        <NavLink to={"/register"} className="nav-link">
                            Sign Up
                        </NavLink>
                    </li>
                </div>
            )}
        </nav>
    )
}

export default Navbar
