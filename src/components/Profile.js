import React, {useEffect, useState} from "react";
import AuthService from "../services/auth.service";
import GrindDataService from "../services/grind.service";
import CharacterDataService from "../services/character.service";
import {Redirect} from "react-router-dom";
import {Bar} from "react-chartjs-2";
import DeleteConfirmation from "./DeleteConfirmation";

/**
 * User Profile page
 * @returns {JSX.Element}
 * @constructor
 */
const Profile = () => {
    const currentUser = AuthService.getCurrentUser();
    const [grindSessions, setGrindSessions] = useState([]);
    const [id, setId] = useState([]);
    const [type, setType] = useState([]);
    const [averagesPerGrindspot, setAveragesPerGrindspot] = useState([]);
    const [characters, setCharacters] = useState([]);
    const [createRedirect, setCreateRedirect] = useState(false);
    const [createCharRedirect, setCreateCharRedirect] = useState(false);
    const [characterRedirect, setCharacterRedirect] = useState("");
    const [grindRedirect, setGrindRedirect] = useState("");
    const [displayConfirmationModal, setDisplayConfirmationModal] = useState(false);
    const [deleteMessage, setDeleteMessage] = useState(null);
    const [characterMessage, setCharacterMessage] = useState(null);
    const [grindMessage, setGrindMessage] = useState(null);

    useEffect(() => {
        // Retrieve All grind sessions and characters
        if (currentUser) {
            getGrinds();
            getCharacters();
            getAveragesPerGrindspot();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


    const optionsAveragePerGrindspot = {
        plugins: {
            legend: {
                display: false
            }
        },
        scaleShowValues: true,
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
            xAxes: [
                {
                    ticks: {
                        autoSkip: false,
                    },
                },
            ],
        },
    };

    /**
     * Handle the displaying of the modal based on type and id
     *
     * @param type Type of the object to delete (grind or character)
     * @param id Unique identifier of the object to delete
     */
    const showDeleteModal = (type, id) => {
        setType(type);
        setId(id);
        setCharacterMessage(null);
        setGrindMessage(null);

        if (type === "character") {
            setDeleteMessage(`Are you sure you want to delete the character '${characters.find((x) => x.id === id).name}' and all it's grind sessions ?`);
        } else if (type === "grind") {
            setDeleteMessage(`Are you sure you want to delete this grind sessions ?`);
        }

        setDisplayConfirmationModal(true);
    };

    /**
     * Hide the modal
     */
    const hideConfirmationModal = () => {
        setDisplayConfirmationModal(false);
    };

    /**
     * Retrieve user Grind sessions
     */
    function getGrinds() {
        GrindDataService.getByUser(currentUser.username)
            .then(response => {
                setGrindSessions(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Retrieve average money per grindspot for current user and populate the graph
     */
    function getAveragesPerGrindspot() {
        GrindDataService.getAveragePerGrindspot()
            .then(response => {
                let grindspots = [];
                let data = [];

                for (let value of Object.values(response.data)) {
                    grindspots.push(value.grindspot.name);
                    data.push(value.average);
                }

                setAveragesPerGrindspot({
                    labels: grindspots,
                    datasets: [
                        {
                            label: 'Average Money per Hour',
                            data: data,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                            ],
                            borderWidth: 1,
                        },
                    ],
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Retrieve all Characters for current user
     */
    function getCharacters() {
        CharacterDataService.getByUser(currentUser.username)
            .then(response => {
                setCharacters(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Allow user to delete his character
     *
     * @param characterId Unique identifier of the character to delete
     */
    function deleteCharacter(characterId) {
        CharacterDataService.delete(characterId)
            .then(response => {
                // Once we receive a successfull response we refresh the characters list
                getCharacters();
                // We also refresh the grinds list if the character had any grinds linked to him
                getGrinds();
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Redirect to the proper delete function
     */
    function handeDelete() {
        if (type.toString() === "character") {
            deleteCharacter(id);
            setCharacterMessage(`The character '${characters.find((x) => x.id === id).name}' was deleted successfully.`);
        } else if (type.toString() === "grind") {
            deleteGrind(id);
            setGrindMessage(`The grind session was deleted successfully.`);
        }

        // Close the modal
        hideConfirmationModal();
    }

    /**
     * Allow user to delete his grind session
     *
     * @param grindId Unique identifier of the Grind session to delete
     */
    function deleteGrind(grindId) {
        GrindDataService.delete(grindId)
            .then(response => {
                // Once we receive a successfull response we refresh the grinds list
                getGrinds();
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        <div>
            {/* Redirect to login page if unauthorized */}
            {currentUser == null && (
                <Redirect to={{pathname: '/unauthorized'}}/>
            )}

            {/* Redirect to add Grind component */}
            {createRedirect && (
                <div>
                    <Redirect to={{pathname: '/addGrind'}}/>
                </div>
            )}

            {createCharRedirect && (
                <div>
                    <Redirect to={{pathname: '/addCharacter'}}/>
                </div>
            )}

            {characterRedirect && (
                <div>
                    <Redirect to={{
                        pathname: characterRedirect
                    }}/>
                </div>
            )}

            {grindRedirect && (
                <div>
                    <Redirect to={{
                        pathname: grindRedirect
                    }}/>
                </div>
            )}


            <header className="page-header">
                <h1><strong>{currentUser?.username}</strong> Profile</h1>
            </header>

            <div className={"block"}>
                <h4>Average Money per hour per Grindspot</h4>
                <Bar data={averagesPerGrindspot} options={optionsAveragePerGrindspot}/>
            </div>

            <div className={"block no-background"}>
                <h4>Characters</h4>

                {characterMessage &&
                <div className="block text-success" role={"alert"}>{characterMessage}</div>
                }

                {characters.length > 0 &&
                <div className={"table-scroll"}>
                    <table className={"table table-bordered table-striped table-dark table-hover-purple"}>
                        <thead className={"thead-colored"}>
                        <tr>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Level</th>
                            <th>AP</th>
                            <th>AAP</th>
                            <th>DP</th>
                            <th>Off-hand</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {characters &&
                        characters.map((character) => (
                            <tr key={character.id}>
                                <td>{character.name}</td>
                                <td>{character.classe.name}</td>
                                <td>{character.level}</td>
                                <td>{character.ap}</td>
                                <td>{character.aap}</td>
                                <td>{character.dp}</td>

                                {/* CONDITIONAL RENDERING ON KUTUM*/}
                                {character.kutum === 0 &&
                                <td>Nouver</td>
                                }
                                {character.kutum === 1 &&
                                <td>TET Kutum</td>
                                }
                                {character.kutum === 2 &&
                                <td>PEN Kutum</td>
                                }

                                <td className={"actions-cell"}>
                                    {/* REDIRECT TO EDIT */}
                                    <i onClick={() => setCharacterRedirect("/characters/edit/" + character.id)}
                                       className="bi bi-pencil-fill text-warning"/>
                                    {/* Icon used as a button to remove the items */}
                                    <i className="fas fa-trash-alt red-text"
                                       onClick={() => showDeleteModal("character", character.id)}/>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
                }

                {characters.length === 0 &&
                <div className="block text-center" role="alert">
                    <strong>Whoops ! </strong>
                    <span>You currently don't have any <strong>Character</strong>. Create one to start seeing this section !</span>
                </div>
                }

                <button className={"button-circle"} onClick={() => setCreateCharRedirect(true)}><i
                    className="bi bi-plus-lg"/>
                </button>
            </div>


            <div className={"block no-background"}>
                <h4>Grind sessions</h4>

                {grindMessage &&
                <div className="block text-success" role={"alert"}>{grindMessage}</div>
                }

                {grindSessions.length > 0 &&
                <div className={"table-scroll"}>
                    <table className={"table table-bordered table-striped table-dark table-hover-purple"}>
                        <thead className={"thead-colored"}>
                        <tr>
                            <th>Grindspot</th>
                            <th>Character</th>
                            <th>Class</th>
                            <th>AP</th>
                            <th>AAP</th>
                            <th>DP</th>
                            <th>Off-hand</th>
                            <th>Duration</th>
                            <th>Silver</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {grindSessions &&
                        grindSessions.map((grindSession) => (
                            <tr key={grindSession.id}>
                                <td>{grindSession.grindspot.name}</td>
                                <td>{grindSession.c_character.name}</td>
                                <td>{grindSession.c_character.classe.name}</td>
                                <td>{grindSession.c_character.ap}</td>
                                <td>{grindSession.c_character.aap}</td>
                                <td>{grindSession.c_character.dp}</td>

                                {/* CONDITIONAL RENDERING ON KUTUM*/}
                                {grindSession.c_character.kutum === 0 &&
                                <td>Nouver</td>
                                }
                                {grindSession.c_character.kutum === 1 &&
                                <td>TET Kutum</td>
                                }
                                {grindSession.c_character.kutum === 2 &&
                                <td>PEN Kutum</td>
                                }

                                <td>{Math.floor(grindSession.duration / (1000 * 60 * 60))}
                                    h
                                    {Math.floor(grindSession.duration / (1000 * 60)) % 60}
                                    m
                                    {Math.floor(grindSession.duration / 1000) % 60}
                                    s
                                </td>
                                <td>{grindSession.totalSilver.toLocaleString()}</td>

                                <td className={"actions-cell"}>
                                    {/* REDIRECT TO SHOW */}
                                    <i className="bi bi-eye-fill"
                                       onClick={() => setGrindRedirect("/grinds/show/" + grindSession.id)}
                                    />
                                    {/* REDIRECT TO EDIT */}
                                    <i className="bi bi-pencil-fill text-warning"
                                       onClick={() => setGrindRedirect("/grinds/edit/" + grindSession.id)}
                                    />
                                    {/* Icon used as a button to remove the items */}
                                    <i className="fas fa-trash-alt red-text"
                                       onClick={() => showDeleteModal("grind", grindSession.id)}/>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
                }
                {grindSessions.length === 0 &&
                <div className="block text-center" role="alert">
                    <strong>Whoops ! </strong>
                    <span>You currently don't have any <strong>Grind Session</strong>. Create one to start seeing this section !</span>
                </div>
                }

                <button className={"button-circle"} onClick={() => setCreateRedirect(true)}><i
                    className="bi bi-plus-lg"/>
                </button>
            </div>
            <DeleteConfirmation showModal={displayConfirmationModal} confirmModal={handeDelete}
                                hideModal={hideConfirmationModal} type={type} id={id} message={deleteMessage}/>
        </div>
    );
};

export default Profile;
