import React, {Component} from "react";
import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";
import ItemDataService from "../services/item.service";

// Import Constants
import * as bossMainhand from '../constants/bossMainhand';
import * as bossAwakening from '../constants/bossAwakening';
import * as bossArmor from '../constants/bossArmor';
import * as greenArmor from '../constants/greenArmor';
import * as dimTreeSpiritArmor from '../constants/dimTreeSpiritArmor';
import * as bossOffhand from '../constants/bossOffhand';
import silver from "../assets/images/icons/silver.png";

/**
 * Caphras stone calculator
 * This component is used to calculate the gains in statistic, costs and stone requirement
 * between each Caphras level of multiple items
 */
export default class CaphrasCalculator extends Component {
    constructor(props) {
        super(props);

        this.retrieveItems = this.retrieveItems.bind(this);
        this.onChangeCaphrasPrice = this.onChangeCaphrasPrice.bind(this);
        this.onChangeCurrentCaphrasLevel = this.onChangeCurrentCaphrasLevel.bind(this);
        this.onChangeWantedCaphrasLevel = this.onChangeWantedCaphrasLevel.bind(this);
        this.calculateStats = this.calculateStats.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.increaseCurrent = this.increaseCurrent.bind(this);
        this.decreaseCurrent = this.decreaseCurrent.bind(this);
        this.increaseWanted = this.increaseWanted.bind(this);
        this.decreaseWanted = this.decreaseWanted.bind(this);


        this.state = {
            // Items needed
            caphrasStone: {
                name: '',
                price: 0,
                icon: '',
            },

            currentCaphrasLevel: 0,
            wantedCaphrasLevel: 20,


            // Initiate item to 0
            bossArmor: {
                AP: 0,
                DP: 0,
                Evasion: 0,
                DR: 0,
                Accuracy: 0,
                HiddenEvasion: 0,
                HiddenDamageReduction: 0,
                HP: 0,
                StonesQuantity: 0,
                Cost: 0,
            },

            bossAwakening: {
                AP: 0,
                DP: 0,
                Evasion: 0,
                DR: 0,
                Accuracy: 0,
                HiddenEvasion: 0,
                HiddenDamageReduction: 0,
                HP: 0,
                StonesQuantity: 0,
                Cost: 0,
            },

            bossMainhand: {
                AP: 0,
                DP: 0,
                Evasion: 0,
                DR: 0,
                Accuracy: 0,
                HiddenEvasion: 0,
                HiddenDamageReduction: 0,
                HP: 0,
                StonesQuantity: 0,
                Cost: 0,
            },

            bossOffhand: {
                AP: 0,
                DP: 0,
                Evasion: 0,
                DR: 0,
                Accuracy: 0,
                HiddenEvasion: 0,
                HiddenDamageReduction: 0,
                HP: 0,
                StonesQuantity: 0,
                Cost: 0,
            },

            dimTreeSpiritArmor: {
                AP: 0,
                DP: 0,
                Evasion: 0,
                DR: 0,
                Accuracy: 0,
                HiddenEvasion: 0,
                HiddenDamageReduction: 0,
                HP: 0,
                StonesQuantity: 0,
                Cost: 0,
            },

            greenArmor: {
                AP: 0,
                DP: 0,
                Evasion: 0,
                DR: 0,
                Accuracy: 0,
                HiddenEvasion: 0,
                HiddenDamageReduction: 0,
                HP: 0,
                StonesQuantity: 0,
                Cost: 0,
            },

            authorized: true,

            submitted: false
        };
    }

    componentDidMount() {
        // Check auth
        this.checkAuth();

        // Retrieve items infos
        this.retrieveItems();
    }

    /**
     * Calculates Items stats + sets caphras quantity and price
     */
    calculateStats() {
        let bossArmorCalc = this.state.bossArmor;
        let bossAwakeningCalc = this.state.bossAwakening;
        let bossMainhandCalc = this.state.bossMainhand;
        let bossOffhandCalc = this.state.bossOffhand;
        let dimTreeSpiritArmorCalc = this.state.dimTreeSpiritArmor;
        let greenArmorCalc = this.state.greenArmor;


        bossArmorCalc.AP = 0;
        bossArmorCalc.DP = 0;
        bossArmorCalc.Evasion = 0;
        bossArmorCalc.DR = 0;
        bossArmorCalc.Accuracy = 0;
        bossArmorCalc.HiddenEvasion = 0;
        bossArmorCalc.HiddenDamageReduction = 0;
        bossArmorCalc.HP = 0;
        bossArmorCalc.StonesQuantity = 0;

        bossAwakeningCalc.AP = 0;
        bossAwakeningCalc.DP = 0;
        bossAwakeningCalc.Evasion = 0;
        bossAwakeningCalc.DR = 0;
        bossAwakeningCalc.Accuracy = 0;
        bossAwakeningCalc.HiddenEvasion = 0;
        bossAwakeningCalc.HiddenDamageReduction = 0;
        bossAwakeningCalc.HP = 0;
        bossAwakeningCalc.StonesQuantity = 0;

        bossMainhandCalc.AP = 0;
        bossMainhandCalc.DP = 0;
        bossMainhandCalc.Evasion = 0;
        bossMainhandCalc.DR = 0;
        bossMainhandCalc.Accuracy = 0;
        bossMainhandCalc.HiddenEvasion = 0;
        bossMainhandCalc.HiddenDamageReduction = 0;
        bossMainhandCalc.HP = 0;
        bossMainhandCalc.StonesQuantity = 0;

        bossOffhandCalc.AP = 0;
        bossOffhandCalc.DP = 0;
        bossOffhandCalc.Evasion = 0;
        bossOffhandCalc.DR = 0;
        bossOffhandCalc.Accuracy = 0;
        bossOffhandCalc.HiddenEvasion = 0;
        bossOffhandCalc.HiddenDamageReduction = 0;
        bossOffhandCalc.HP = 0;
        bossOffhandCalc.StonesQuantity = 0;

        dimTreeSpiritArmorCalc.AP = 0;
        dimTreeSpiritArmorCalc.DP = 0;
        dimTreeSpiritArmorCalc.Evasion = 0;
        dimTreeSpiritArmorCalc.DR = 0;
        dimTreeSpiritArmorCalc.Accuracy = 0;
        dimTreeSpiritArmorCalc.HiddenEvasion = 0;
        dimTreeSpiritArmorCalc.HiddenDamageReduction = 0;
        dimTreeSpiritArmorCalc.HP = 0;
        dimTreeSpiritArmorCalc.StonesQuantity = 0;

        greenArmorCalc.AP = 0;
        greenArmorCalc.DP = 0;
        greenArmorCalc.Evasion = 0;
        greenArmorCalc.DR = 0;
        greenArmorCalc.Accuracy = 0;
        greenArmorCalc.HiddenEvasion = 0;
        greenArmorCalc.HiddenDamageReduction = 0;
        greenArmorCalc.HP = 0;
        greenArmorCalc.StonesQuantity = 0;


        if (this.state.currentCaphrasLevel !== this.state.wantedCaphrasLevel) {
            // Update Stats
            for (let i = parseInt(this.state.currentCaphrasLevel) + 1; i <= this.state.wantedCaphrasLevel; i++) {
                bossArmorCalc.AP += bossArmor["LEVEL" + i].AP;
                bossArmorCalc.DP += bossArmor["LEVEL" + i].DP;
                bossArmorCalc.Evasion += bossArmor["LEVEL" + i].Evasion;
                bossArmorCalc.DR += bossArmor["LEVEL" + i].DR;
                bossArmorCalc.Accuracy += bossArmor["LEVEL" + i].Accuracy;
                bossArmorCalc.HiddenEvasion += bossArmor["LEVEL" + i].HiddenEvasion;
                bossArmorCalc.HiddenDamageReduction += bossArmor["LEVEL" + i].HiddenDamageReduction;
                bossArmorCalc.HP += bossArmor["LEVEL" + i].HP;
                bossArmorCalc.StonesQuantity += bossArmor["LEVEL" + i].StonesQuantity;

                bossAwakeningCalc.AP += bossAwakening["LEVEL" + i].AP;
                bossAwakeningCalc.DP += bossAwakening["LEVEL" + i].DP;
                bossAwakeningCalc.Evasion += bossAwakening["LEVEL" + i].Evasion;
                bossAwakeningCalc.DR += bossAwakening["LEVEL" + i].DR;
                bossAwakeningCalc.Accuracy += bossAwakening["LEVEL" + i].Accuracy;
                bossAwakeningCalc.HiddenEvasion += bossAwakening["LEVEL" + i].HiddenEvasion;
                bossAwakeningCalc.HiddenDamageReduction += bossAwakening["LEVEL" + i].HiddenDamageReduction;
                bossAwakeningCalc.HP += bossAwakening["LEVEL" + i].HP;
                bossAwakeningCalc.StonesQuantity += bossAwakening["LEVEL" + i].StonesQuantity;

                bossMainhandCalc.AP += bossMainhand["LEVEL" + i].AP;
                bossMainhandCalc.DP += bossMainhand["LEVEL" + i].DP;
                bossMainhandCalc.Evasion += bossMainhand["LEVEL" + i].Evasion;
                bossMainhandCalc.DR += bossMainhand["LEVEL" + i].DR;
                bossMainhandCalc.Accuracy += bossMainhand["LEVEL" + i].Accuracy;
                bossMainhandCalc.HiddenEvasion += bossMainhand["LEVEL" + i].HiddenEvasion;
                bossMainhandCalc.HiddenDamageReduction += bossMainhand["LEVEL" + i].HiddenDamageReduction;
                bossMainhandCalc.HP += bossMainhand["LEVEL" + i].HP;
                bossMainhandCalc.StonesQuantity += bossMainhand["LEVEL" + i].StonesQuantity;

                bossOffhandCalc.AP += bossOffhand["LEVEL" + i].AP;
                bossOffhandCalc.DP += bossOffhand["LEVEL" + i].DP;
                bossOffhandCalc.Evasion += bossOffhand["LEVEL" + i].Evasion;
                bossOffhandCalc.DR += bossOffhand["LEVEL" + i].DR;
                bossOffhandCalc.Accuracy += bossOffhand["LEVEL" + i].Accuracy;
                bossOffhandCalc.HiddenEvasion += bossOffhand["LEVEL" + i].HiddenEvasion;
                bossOffhandCalc.HiddenDamageReduction += bossOffhand["LEVEL" + i].HiddenDamageReduction;
                bossOffhandCalc.HP += bossOffhand["LEVEL" + i].HP;
                bossOffhandCalc.StonesQuantity += bossOffhand["LEVEL" + i].StonesQuantity;

                dimTreeSpiritArmorCalc.AP += dimTreeSpiritArmor["LEVEL" + i].AP;
                dimTreeSpiritArmorCalc.DP += dimTreeSpiritArmor["LEVEL" + i].DP;
                dimTreeSpiritArmorCalc.Evasion += dimTreeSpiritArmor["LEVEL" + i].Evasion;
                dimTreeSpiritArmorCalc.DR += dimTreeSpiritArmor["LEVEL" + i].DR;
                dimTreeSpiritArmorCalc.Accuracy += dimTreeSpiritArmor["LEVEL" + i].Accuracy;
                dimTreeSpiritArmorCalc.HiddenEvasion += dimTreeSpiritArmor["LEVEL" + i].HiddenEvasion;
                dimTreeSpiritArmorCalc.HiddenDamageReduction += dimTreeSpiritArmor["LEVEL" + i].HiddenDamageReduction;
                dimTreeSpiritArmorCalc.HP += dimTreeSpiritArmor["LEVEL" + i].HP;
                dimTreeSpiritArmorCalc.StonesQuantity += dimTreeSpiritArmor["LEVEL" + i].StonesQuantity;

                greenArmorCalc.AP += greenArmor["LEVEL" + i].AP;
                greenArmorCalc.DP += greenArmor["LEVEL" + i].DP;
                greenArmorCalc.Evasion += greenArmor["LEVEL" + i].Evasion;
                greenArmorCalc.DR += greenArmor["LEVEL" + i].DR;
                greenArmorCalc.Accuracy += greenArmor["LEVEL" + i].Accuracy;
                greenArmorCalc.HiddenEvasion += greenArmor["LEVEL" + i].HiddenEvasion;
                greenArmorCalc.HiddenDamageReduction += greenArmor["LEVEL" + i].HiddenDamageReduction;
                greenArmorCalc.HP += greenArmor["LEVEL" + i].HP;
                greenArmorCalc.StonesQuantity += greenArmor["LEVEL" + i].StonesQuantity;
            }
        }


        // Update costs
        bossArmorCalc.Cost = this.state.caphrasStone.price * bossArmorCalc.StonesQuantity;
        bossAwakeningCalc.Cost = this.state.caphrasStone.price * bossAwakeningCalc.StonesQuantity;
        bossMainhandCalc.Cost = this.state.caphrasStone.price * bossMainhandCalc.StonesQuantity;
        bossOffhandCalc.Cost = this.state.caphrasStone.price * bossOffhandCalc.StonesQuantity;
        dimTreeSpiritArmorCalc.Cost = this.state.caphrasStone.price * dimTreeSpiritArmorCalc.StonesQuantity;
        greenArmorCalc.Cost = this.state.caphrasStone.price * greenArmorCalc.StonesQuantity;

        this.setState({
            bossArmor: bossArmorCalc,
            bossAwakening: bossAwakeningCalc,
            bossMainhand: bossMainhandCalc,
            bossOffhand: bossOffhandCalc,
            dimTreeSpiritArmor: dimTreeSpiritArmorCalc,
            greenArmor: greenArmorCalc,
        });
    }

    onChangeCaphrasPrice(e) {
        let caphrasStone = this.state.caphrasStone;

        let newValue = e.target.value;

        if (newValue < 0) {
            newValue = 0;
        }

        caphrasStone.price = newValue;

        this.setState({
                caphrasStone: caphrasStone,
            },
            () => {
                // Recalculate total price + stats
                this.calculateStats();
            }
        );
    }

    onChangeWantedCaphrasLevel(e) {
        let newValue = 0;
        // When user empties the input
        if (e.target.value === "" || e.target.value <= this.state.currentCaphrasLevel) {
            newValue = parseInt(this.state.currentCaphrasLevel) + 1;
        } else if (e.target.value > 20) {
            newValue = 20;
        } else {
            newValue = e.target.value;
        }

        this.setState({
                wantedCaphrasLevel: newValue,
            },
            () => {
                // Recalculate total price + stats
                this.calculateStats();
            }
        );
    }

    onChangeCurrentCaphrasLevel(e) {
        let newValue = 0;
        // When user empties the input
        if (e.target.value === "" || e.target.value < 0) {
            newValue = 0;
        } else if (e.target.value >= this.state.wantedCaphrasLevel) {
            newValue = this.state.wantedCaphrasLevel - 1;
        } else {
            newValue = e.target.value;
        }

        this.setState({
                currentCaphrasLevel: newValue,
            },
            () => {
                // Recalculate total price + stats
                this.calculateStats();
            }
        );
    }

    /**
     * Increase caphras current level by 1
     */
    increaseCurrent() {
        if (parseInt(this.state.currentCaphrasLevel) + 1 < this.state.wantedCaphrasLevel) {
            this.setState({
                    currentCaphrasLevel: parseInt(this.state.currentCaphrasLevel) + 1,
                },
                () => {
                    // Recalculate total price + stats
                    this.calculateStats();
                }
            );
        }
    }

    /**
     * Decreases caphras current level by 1
     */
    decreaseCurrent() {
        if (this.state.currentCaphrasLevel - 1 >= 0) {
            this.setState({
                    currentCaphrasLevel: parseInt(this.state.currentCaphrasLevel) - 1,
                },
                () => {
                    // Recalculate total price + stats
                    this.calculateStats();
                }
            );
        }
    }

    /**
     * Increases caphras wanted level by 1
     */
    increaseWanted() {
        if (parseInt(this.state.wantedCaphrasLevel) + 1 <= 20) {
            this.setState({
                    wantedCaphrasLevel: parseInt(this.state.wantedCaphrasLevel) + 1,
                },
                () => {
                    // Recalculate total price + stats
                    this.calculateStats();
                }
            );
        }
    }

    /**
     * Decreases caphras wanted level by 1
     */
    decreaseWanted() {
        if (this.state.wantedCaphrasLevel - 1 >= 0 && this.state.wantedCaphrasLevel - 1 > this.state.currentCaphrasLevel) {
            this.setState({
                    wantedCaphrasLevel: this.state.wantedCaphrasLevel - 1,
                },
                () => {
                    // Recalculate total price + stats
                    this.calculateStats();
                }
            );
        }
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getUserBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }


    /**
     * Retrieve infos about the items required for this calculator
     */
    retrieveItems() {
        ItemDataService.findByName("Caphras Stone")
            .then(response => {
                // Set default values for qty
                let caphrasStone = response.data[0];
                caphrasStone.quantity = 0;

                this.setState({
                        caphrasStone: caphrasStone
                    },
                    () => {
                        this.calculateStats();
                    }
                );
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        return (
            <div className={"page-caphras"}>
                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}
                <div>
                    <header className="page-header">
                        <h1>Caphras Calculator</h1>
                    </header>

                    <div className={"block no-background"}>
                        <div className={"block block-caphras"}>

                            <h4>
                                {this.state.caphrasStone.name}
                            </h4>

                            <div className={"corrupted-icon-container"}>
                                <div>
                                    {this.state.caphrasStone.icon &&
                                    <img className={"corrupted-icon"} alt="Caphras icon"
                                         src={"data:image/jpeg;base64," + this.state.caphrasStone.icon}/>
                                    }
                                </div>
                            </div>

                            {/*  Form quantity  */}
                            <div className={"container-price"}>
                                <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                <input type="number" id={"caphras-price"} name="price"
                                       className="input-outlined input-outlined-price"
                                       value={this.state.caphrasStone.price}
                                       min={"0"}
                                       onChange={this.onChangeCaphrasPrice}/>
                            </div>

                            <div className={"container-level"}>
                                {/* Form Current Level */}
                                <label className={"caphras-labels"} htmlFor={"caphras-current-level"}>
                                    Current Caphras Level
                                </label>

                                <div className={"increase-descrease"}>
                                    <button className={"button-circle button-circle--small"}
                                            onClick={this.decreaseCurrent}>
                                        <i className="bi bi-dash"/>
                                    </button>
                                    <input className={"input-outlined"} type="number" id={"caphras-current-level"}
                                           name="caphras-current-level"
                                           value={this.state.currentCaphrasLevel}
                                           min={"0"}
                                           max={parseInt(this.state.wantedCaphrasLevel) - 1}
                                           onChange={this.onChangeCurrentCaphrasLevel}/>

                                    <button className={"button-circle button-circle--small"}
                                            onClick={this.increaseCurrent}>
                                        <i className="bi bi-plus"/>
                                    </button>
                                </div>


                                {/* Form Wanted Level */}
                                <label className={"caphras-labels"} htmlFor={"caphras-wanted-level"}>Wanted Caphras
                                    Level</label>

                                <div className={"increase-descrease"}>
                                    <button className={"button-circle button-circle--small"}
                                            onClick={this.decreaseWanted}>
                                        <i className="bi bi-dash"/>
                                    </button>
                                    <input className={"input-outlined"} type="number" id={"caphras-wanted-level"}
                                           name="caphras-wanted-level"
                                           value={this.state.wantedCaphrasLevel}
                                           min={parseInt(this.state.currentCaphrasLevel) + 1}
                                           max={"20"}
                                           onChange={this.onChangeWantedCaphrasLevel}/>

                                    <button className={"button-circle button-circle--small"}
                                            onClick={this.increaseWanted}>
                                        <i className="bi bi-plus"/>
                                    </button>
                                </div>


                            </div>
                        </div>
                        <div className={"table-scroll"}>
                            <table className={"table table-bordered table-dark table-striped table-hover-purple"}>
                                <thead className={"thead-colored"}>
                                <tr>
                                    <th>Item</th>
                                    <th>AP</th>
                                    <th>DP</th>
                                    <th>Evasion</th>
                                    <th>DR</th>
                                    <th>Accuracy</th>
                                    <th>Hidden Evasion</th>
                                    <th>Hidden DR</th>
                                    <th>HP</th>
                                    <th>Stones Needed</th>
                                    <th>Cost</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th>Boss Mainhand V</th>
                                    <td>{this.state.bossMainhand.AP}</td>
                                    <td>{this.state.bossMainhand.DP}</td>
                                    <td>{this.state.bossMainhand.Evasion}</td>
                                    <td>{this.state.bossMainhand.DR}</td>
                                    <td>{this.state.bossMainhand.Accuracy}</td>
                                    <td>{this.state.bossMainhand.HiddenEvasion}</td>
                                    <td>{this.state.bossMainhand.HiddenDamageReduction}</td>
                                    <td>{this.state.bossMainhand.HP}</td>
                                    <td>{this.state.bossMainhand.StonesQuantity.toLocaleString()}</td>
                                    <td>{this.state.bossMainhand.Cost.toLocaleString()}</td>
                                </tr>
                                <tr>
                                    <th>Boss Awakening V</th>
                                    <td>{this.state.bossAwakening.AP}</td>
                                    <td>{this.state.bossAwakening.DP}</td>
                                    <td>{this.state.bossAwakening.Evasion}</td>
                                    <td>{this.state.bossAwakening.DR}</td>
                                    <td>{this.state.bossAwakening.Accuracy}</td>
                                    <td>{this.state.bossAwakening.HiddenEvasion}</td>
                                    <td>{this.state.bossAwakening.HiddenDamageReduction}</td>
                                    <td>{this.state.bossAwakening.HP}</td>
                                    <td>{this.state.bossAwakening.StonesQuantity.toLocaleString()}</td>
                                    <td>{this.state.bossAwakening.Cost.toLocaleString()}</td>
                                </tr>
                                <tr>
                                    <th>Boss Offhand V</th>
                                    <td>{this.state.bossOffhand.AP}</td>
                                    <td>{this.state.bossOffhand.DP}</td>
                                    <td>{this.state.bossOffhand.Evasion}</td>
                                    <td>{this.state.bossOffhand.DR}</td>
                                    <td>{this.state.bossOffhand.Accuracy}</td>
                                    <td>{this.state.bossOffhand.HiddenEvasion}</td>
                                    <td>{this.state.bossOffhand.HiddenDamageReduction}</td>
                                    <td>{this.state.bossOffhand.HP}</td>
                                    <td>{this.state.bossOffhand.StonesQuantity.toLocaleString()}</td>
                                    <td>{this.state.bossOffhand.Cost.toLocaleString()}</td>
                                </tr>
                                <tr>
                                    <th>Boss Armor V</th>
                                    <td>{this.state.bossArmor.AP}</td>
                                    <td>{this.state.bossArmor.DP}</td>
                                    <td>{this.state.bossArmor.Evasion}</td>
                                    <td>{this.state.bossArmor.DR}</td>
                                    <td>{this.state.bossArmor.Accuracy}</td>
                                    <td>{this.state.bossArmor.HiddenEvasion}</td>
                                    <td>{this.state.bossArmor.HiddenDamageReduction}</td>
                                    <td>{this.state.bossArmor.HP}</td>
                                    <td>{this.state.bossArmor.StonesQuantity.toLocaleString()}</td>
                                    <td>{this.state.bossArmor.Cost.toLocaleString()}</td>
                                </tr>
                                <tr>
                                    <th>Dim Tree Spirit's Armor V</th>
                                    <td>{this.state.dimTreeSpiritArmor.AP}</td>
                                    <td>{this.state.dimTreeSpiritArmor.DP}</td>
                                    <td>{this.state.dimTreeSpiritArmor.Evasion}</td>
                                    <td>{this.state.dimTreeSpiritArmor.DR}</td>
                                    <td>{this.state.dimTreeSpiritArmor.Accuracy}</td>
                                    <td>{this.state.dimTreeSpiritArmor.HiddenEvasion}</td>
                                    <td>{this.state.dimTreeSpiritArmor.HiddenDamageReduction}</td>
                                    <td>{this.state.dimTreeSpiritArmor.HP}</td>
                                    <td>{this.state.dimTreeSpiritArmor.StonesQuantity.toLocaleString()}</td>
                                    <td>{this.state.dimTreeSpiritArmor.Cost.toLocaleString()}</td>
                                </tr>
                                <tr>
                                    <th>Green Armor V</th>
                                    <td>{this.state.greenArmor.AP}</td>
                                    <td>{this.state.greenArmor.DP}</td>
                                    <td>{this.state.greenArmor.Evasion}</td>
                                    <td>{this.state.greenArmor.DR}</td>
                                    <td>{this.state.greenArmor.Accuracy}</td>
                                    <td>{this.state.greenArmor.HiddenEvasion}</td>
                                    <td>{this.state.greenArmor.HiddenDamageReduction}</td>
                                    <td>{this.state.greenArmor.HP}</td>
                                    <td>{this.state.greenArmor.StonesQuantity.toLocaleString()}</td>
                                    <td>{this.state.greenArmor.Cost.toLocaleString()}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
