import React, {Component} from "react";
import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";
import valuePack from "../assets/images/icons/value-pack.png";
import Switch from "react-switch";
import silver from "../assets/images/icons/silver.png";
import ring from "../assets/images/icons/ring.png";

/**
 * Tax Calculator
 * Used to Calculate the marketplace tax and see a breakdown
 */
export default class Tax extends Component {
    constructor(props) {
        super(props);

        this.changeValuePack = this.changeValuePack.bind(this);
        this.changeMerchantRing = this.changeMerchantRing.bind(this);
        this.onChangeSellPrice = this.onChangeSellPrice.bind(this);
        this.onChangeFamilyFame = this.onChangeFamilyFame.bind(this);
        this.recalculate = this.recalculate.bind(this);
        this.checkAuth = this.checkAuth.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);

        this.state = {
            // Options modifiers
            valuePack: false,
            merchantRing: false,
            familyFame: 0,
            sellPrice: 100000,

            // Calculated values
            profit: 65000, // We set the first calculation default value

            authorized: true,
        };
    }

    componentDidMount() {
        // Check auth
        this.checkAuth();
    }


    /**
     * Inverts value pack value
     */
    changeValuePack() {
        this.setState({valuePack: !this.state.valuePack}, () => {
            this.recalculate();
        });
    }

    /**
     * Inverts Merchant's ring value
     */
    changeMerchantRing() {
        this.setState({merchantRing: !this.state.merchantRing}, () => {
            this.recalculate();
        });
    }

    /**
     * Calculate / recalculates the tax values
     */
    recalculate() {
        let taxes = this.state.sellPrice * 0.35;
        let bonus = 0;

        if (this.state.valuePack) {
            bonus += Math.round(this.state.sellPrice * 0.845 - this.state.sellPrice * 0.65);
        }

        if (this.state.merchantRing) {
            bonus += Math.round(this.state.sellPrice * 0.65 * 0.05);
        }

        if (this.state.familyFame >= 7000) {
            bonus += this.state.sellPrice * 0.65 * 0.005;
        }

        if (this.state.familyFame >= 4000) {
            bonus += this.state.sellPrice * 0.65 * 0.005;
        }

        if (this.state.familyFame >= 1000) {
            bonus += this.state.sellPrice * 0.65 * 0.005;
        }

        bonus = Math.round(bonus);

        this.setState({profit: this.state.sellPrice - taxes + bonus});
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getUserBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    onChangeFamilyFame(e) {
        let newValue = e.target.value;

        if (newValue < 0 || newValue > 999999) {
            newValue = 0;
        }

        this.setState({
            familyFame: newValue
        }, () => {
            this.recalculate();
        });
    }

    onChangeSellPrice(e) {
        let newValue = e.target.value;
        if (e.target.value < 0) {
            newValue = 0;
        }
        this.setState({
                sellPrice: newValue
            }, () => {
                this.recalculate();
            }
        );
    }

    render() {
        return (
            <div>
                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}

                <header className="page-header">
                    <h1>Tax Calculator</h1>
                </header>

                <div className="grid-corrupted grid-tax">
                    <div className={"block corrupted-block tax-block"}>
                        <h4>Item Sold</h4>

                        <div className={"options-switches-tax-container"}>
                            <div className={"col1"}>
                                <div className={"corrupted-icon-container"}>
                                    <div>
                                        {this.state.valuePack ? (
                                            <img className={"corrupted-icon"} alt={"Value pack icon"}
                                                 src={valuePack}/>
                                        ) : (
                                            <img className={"greyscale corrupted-icon"} alt={"Value pack icon"}
                                                 src={valuePack}/>
                                        )}
                                    </div>
                                </div>

                                {/* Value pack switch */}
                                <div className={"increase-descrease"}>
                                    <Switch activeBoxShadow={"null"} offColor={"#403F51"} onColor={"#8D00F2"}
                                            onChange={this.changeValuePack}
                                            checked={this.state.valuePack}/>
                                </div>
                            </div>

                            <div className={"col1"}>
                                <div className={"corrupted-icon-container"}>
                                    <div>
                                        {this.state.merchantRing ? (
                                            <img className={"corrupted-icon"} alt={"Value pack icon"}
                                                 src={ring}/>
                                        ) : (
                                            <img className={"greyscale corrupted-icon"} alt={"Value pack icon"}
                                                 src={ring}/>
                                        )}
                                    </div>
                                </div>

                                {/* Merchant ring switch */}
                                <div className={"increase-descrease"}>
                                    <Switch activeBoxShadow={"null"} offColor={"#403F51"} onColor={"#8D00F2"}
                                            onChange={this.changeMerchantRing}
                                            checked={this.state.merchantRing}/>
                                </div>
                            </div>
                        </div>

                        <div className={"container-tax"}>
                            <div className={"grid-container-3"}>
                                <div className={"col1"}>
                                    <label htmlFor="sellPrice">
                                        <span className={"label-tax"}>Sell Price</span>
                                    </label>
                                </div>
                                <div className={"col2 container-price-no-margin"}>
                                    <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                    <input
                                        type="number"
                                        className="input-outlined input-outlined-price"
                                        id="sellPrice"
                                        required
                                        min={"1"}
                                        value={this.state.sellPrice}
                                        onChange={this.onChangeSellPrice}
                                        name="sellPrice"
                                    />
                                </div>
                            </div>
                            <div className={"grid-container-3"}>
                                <div className={"col1"}>
                                    <label htmlFor="familyFame">
                                        <span className={"label-tax"}>Family fame</span>
                                    </label>
                                </div>

                                <div className={"col2 container-price-no-margin"}>
                                    <div className={"silver"}/>
                                    <input
                                        type="number"
                                        className="input-outlined input-outlined-price"
                                        id="familyFame"
                                        min={"0"}
                                        value={this.state.familyFame}
                                        onChange={this.onChangeFamilyFame}
                                        name="familyFame"
                                    />
                                </div>
                            </div>
                        </div>

                    </div>

                    <div className={"add-ingredient"}>
                        <i className="bi bi-arrow-left-right"/>
                    </div>

                    <div className={"block corrupted-block tax-block"}>
                        <h4>Tax Breakdown</h4>

                        <div className={"block block1"}>
                            <div className={"grid-container-2"}>
                                <span className={"col1"}>Sell Price</span>
                                <span
                                    className={"col1 number-tax"}> {(parseInt(this.state.sellPrice)).toLocaleString()}</span>
                            </div>

                            <div className={"grid-container-2 tax-container"}>
                                <span className={"col1"}>Tax</span>
                                <span
                                    className={"col1 text-danger number-tax"}>- {Math.round(this.state.sellPrice * 0.35).toLocaleString()}</span>
                            </div>

                            <div className={"grid-container-2"}>
                                <span className={"col1"}/>
                                <span className={"col1 number-tax operator-tax equal-operator"}>=</span>
                            </div>


                            <div className={"grid-container-2 flex-center"}>
                                <div className={"col1 final-profit"}>
                                    Base Profit
                                </div>
                                <div className={"col1 total-profit container-price"}>
                                    <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                    <div> {Math.round(this.state.sellPrice * 0.65).toLocaleString()}</div>
                                </div>
                            </div>
                        </div>

                        {(this.state.merchantRing || this.state.valuePack || this.state.familyFame >= 1000) && (
                            <div>
                                <div className={"arrow-ingredients line"}>
                                    <i className="bi bi-plus"/>
                                </div>
                                <div className={"block"}>
                                    {/* Display Value Pack Bonus*/}
                                    {this.state.valuePack && (
                                        <div className={"grid-container-2"}>
                                            <span className={"col1"}>Value Pack Bonus</span>
                                            <span className={"col1 number-tax text-success"}>
                                                + {Math.round(this.state.sellPrice * 0.845 - this.state.sellPrice * 0.65).toLocaleString()}
                                            </span>
                                        </div>
                                    )}

                                    {/* Display Merchant's Pack Bonus*/}
                                    {this.state.merchantRing && (
                                        <div className={"grid-container-2"}>
                                            <span className={"col1"}>Merchant's Ring Bonus</span>
                                            <span className={"col1 number-tax text-success"}>
                                                + {Math.round(this.state.sellPrice * 0.65 * 0.05).toLocaleString()}
                                            </span>
                                        </div>
                                    )}


                                    {/* Display Family Fame Bonus*/}
                                    {this.state.familyFame >= 7000 ? (
                                        <div className={"grid-container-2"}>
                                            <span className={"col1"}>Family Fame Bonus</span>
                                            <span className={"col1 number-tax text-success"}>
                                                + {Math.round(this.state.sellPrice * 0.65 * 0.015).toLocaleString()}
                                            </span>
                                        </div>
                                    ) : (
                                        <>
                                            {this.state.familyFame >= 4000 ? (
                                                <div className={"grid-container-2"}>
                                                    <span className={"col1"}>Family Fame Bonus</span>
                                                    <span className={"col1 number-tax text-success"}>
                                                        + {Math.round(this.state.sellPrice * 0.65 * 0.01).toLocaleString()}
                                                    </span>
                                                </div>
                                            ) : (
                                                <>
                                                    {this.state.familyFame >= 1000 && (
                                                        <div className={"grid-container-2"}>
                                                            <span className={"col1"}>Family Fame Bonus</span>
                                                            <span className={"col1 number-tax text-success"}>
                                                                + {Math.round(this.state.sellPrice * 0.65 * 0.005).toLocaleString()}
                                                            </span>
                                                        </div>
                                                    )}
                                                </>
                                            )}
                                        </>
                                    )}
                                </div>
                            </div>
                        )}

                        <div className={"arrow-ingredients line"}>
                            =
                        </div>


                        <div className={"block"}>
                            <div className={"grid-container-2 flex-center"}>
                                <div className={"col1 final-profit"}>
                                    Final Profit
                                </div>
                                <div className={"col1 total-profit container-price"}>
                                    <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                    <div> {Math.round(this.state.profit).toLocaleString()}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
