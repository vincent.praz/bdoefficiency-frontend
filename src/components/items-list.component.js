import React, {Component} from "react";
import ItemDataService from "../services/item.service";
import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";

/**
 * List of all the items
 */
export default class ItemsList extends Component {
    constructor(props) {
        super(props);
        this.onChangeSearchName = this.onChangeSearchName.bind(this);
        this.retrieveItems = this.retrieveItems.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.recalculatePrice = this.recalculatePrice.bind(this);
        this.setActiveItem = this.setActiveItem.bind(this);
        this.searchName = this.searchName.bind(this);

        this.state = {
            items: [],
            currentItem: null,
            currentIndex: -1,
            searchName: "",
            authorized: true,
        };
    }

    componentDidMount() {
        this.checkAuth();
        this.retrieveItems();
    }

    /**
     * Handle the searchbar
     *
     * @param e
     */
    onChangeSearchName(e) {
        const searchName = e.target.value;

        this.setState({
            searchName: searchName
        });

        // Refresh the entire list if the user deletes his search completely
        if (searchName.length === 0) {
            this.refreshList();
        }

        // Execute the search every character typed
        this.searchName();
    }

    /**
     * Retrieve all items
     */
    retrieveItems() {
        ItemDataService.getAll()
            .then(response => {
                this.setState({
                    items: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Refresh list
     */
    refreshList() {
        this.retrieveItems();
        this.setState({
            currentItem: null,
            currentIndex: -1
        });
    }

    /**
     * Set the item to active in the list
     *
     * @param item Item to set active
     * @param index Index of the item
     */
    setActiveItem(item, index) {
        this.setState({
            currentItem: item,
            currentIndex: index
        });
    }

    /**
     * Retrieve items by the search name
     */
    searchName() {
        ItemDataService.findByName(this.state.searchName)
            .then(response => {
                this.setState({
                    items: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Sets the price to the current price of the item in Game
     *
     * @param itemId Unique identifier of the item to refresh price
     */
    recalculatePrice(itemId) {
        ItemDataService.recalculatePrice(itemId)
            .then(response => {
                // Once we receive a successfull response we refresh the items list
                this.refreshList();
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Delete an item
     *
     * @param itemId Unique identifier of the item to delete
     */
    delete(itemId) {
        ItemDataService.delete(itemId)
            .then(response => {
                // Once we receive a successfull response we refresh the items list
                this.refreshList();
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getAdminBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    render() {
        const {searchName, items, currentItem, currentIndex} = this.state;

        return (
            <div className="block">

                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}


                <div className={"grid-container-2"}>
                    <div>
                        <h4>Items List</h4>
                        <div className="">
                            <div className="input-group mb-3 search-grindspot">
                                <input
                                    type="text"
                                    className="input-outlined"
                                    placeholder="Search by name"
                                    value={searchName}
                                    onChange={this.onChangeSearchName}
                                />
                                <div className="input-group-append">
                                    <button
                                        className="button-search"
                                        type="button"
                                        onClick={this.searchName}
                                    >
                                        Search
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div>
                            <ul className="container-group-items">
                                {items &&
                                items.map((item, index) => (
                                    <li
                                        className={
                                            "list-group-item " +
                                            (index === currentIndex ? "active" : "")
                                        }
                                        onClick={() => this.setActiveItem(item, index)}
                                        key={index}
                                    >
                                        {item.icon &&
                                        <img alt="Item icon" src={"data:image/jpeg;base64," + item.icon}/>
                                        }
                                        {item.name}
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>


                    <div>
                        <h4>Item</h4>
                        {currentItem ? (
                            <div className={"block grindspot-preview"}>
                                <div>
                                    <label>
                                        <strong>Icon :</strong>
                                    </label>
                                    {currentItem.icon ? (
                                        <img alt="Current item icon"
                                             src={"data:image/jpeg;base64," + currentItem.icon}/>
                                    ) : (
                                        "No icon"
                                    )}
                                </div>
                                <div>
                                    <label>
                                        <strong>Name :</strong>
                                    </label>
                                    {currentItem.name}
                                </div>
                                <div>
                                    <label>
                                        <strong>API KEY :</strong>
                                    </label>
                                    {currentItem.mainKey}
                                </div>
                                <div>
                                    <label>
                                        <strong>Price :</strong>
                                    </label>
                                    {currentItem.price.toLocaleString()}
                                </div>
                                <div>
                                    <label>
                                        <strong>Taxed :</strong>
                                    </label>
                                    {currentItem.taxed.toString()}
                                </div>
                                {currentItem.taxed === true &&
                                <button
                                    onClick={() => this.recalculatePrice(currentItem.id)}
                                    className="badge badge-warning"
                                >
                                    Recalculate price
                                </button>
                                }
                                <br/>

                                <button
                                    onClick={() => this.delete(currentItem.id)}
                                    className="badge badge-danger"
                                >
                                    Delete
                                </button>
                            </div>
                        ) : (
                            <div>
                                <br/>
                                <p>Please click on a Item...</p>
                            </div>
                        )}
                    </div>
                </div>


            </div>
        );
    }
}
