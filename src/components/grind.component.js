import React, {Component} from "react";
import GrindDataService from "../services/grind.service";
import GrindspotDataService from "../services/grindspot.service";
import CharacterDataService from "../services/character.service";
import Stopwatch from "./Stopwatch";
import Countdown from "./Countdown";
import {Redirect} from "react-router-dom";
import UserService from "../services/user.service";
import AuthService from "../services/auth.service";
import valuePack from "../assets/images/icons/value-pack.png";
import Switch from "react-switch";
import kamasylve from "../assets/images/icons/kamasylve.png";
import dropEvent from "../assets/images/icons/drop-event.png";
import blueScroll from "../assets/images/icons/blue-scroll.png";
import yellowScroll from "../assets/images/icons/yellow-scroll.png";
import agris from "../assets/images/icons/agris.png";
import tent from "../assets/images/icons/tent.png";
import node from "../assets/images/icons/node.png";
import silver from "../assets/images/icons/silver.png";

/**
 * Component used to create a new Grind Session
 */
export default class AddGrind extends Component {
    constructor(props) {
        super(props);

        this.onChangeItemQuantity = this.onChangeItemQuantity.bind(this);
        this.onChangeItemPrice = this.onChangeItemPrice.bind(this);
        this.newGrind = this.newGrind.bind(this);
        this.retrieveGrindspots = this.retrieveGrindspots.bind(this);
        this.retrieveCharacters = this.retrieveCharacters.bind(this);
        this.onChangeGrindspot = this.onChangeGrindspot.bind(this);
        this.onChangeCharacter = this.onChangeCharacter.bind(this);
        this.adjustTimer = this.adjustTimer.bind(this);
        this.changeValuePack = this.changeValuePack.bind(this);
        this.changeKamasylve = this.changeKamasylve.bind(this);
        this.changeTent = this.changeTent.bind(this);
        this.changeYellowScroll = this.changeYellowScroll.bind(this);
        this.changeBlueScroll = this.changeBlueScroll.bind(this);
        this.changeAgris = this.changeAgris.bind(this);
        this.changeDropEvent = this.changeDropEvent.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.startTimer = this.startTimer.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
        this.saveGrind = this.saveGrind.bind(this);
        this.submitGrind = this.submitGrind.bind(this);
        this.checkAuth = this.checkAuth.bind(this);
        this.getGrind = this.getGrind.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.redirectProfile = this.redirectProfile.bind(this);
        this.setGrindRedirect = this.setGrindRedirect.bind(this);

        this.state = {
            id: null,

            // init grind options
            valuePack: false,
            kamasylve: false,
            blueScroll: false,
            yellowScroll: false,
            tent: false,
            agris: false,
            dropEvent: false,
            node: 1,

            authorized: true,

            grindspotsList: [],

            character: {},

            characters: [],

            grindspot: {},

            submitted: false,

            // Timer states
            timerOn: false,
            timerStart: 0,
            timerTime: 3600000,
        };
    }

    componentDidMount() {
        //Scroll to top
        window.scrollTo(0, 0);

        // Check auth
        this.checkAuth();

        // Retrieve all Grindspots
        this.retrieveGrindspots();

        // Retrieve all Characters for current User
        this.retrieveCharacters();

        if (this.props?.match?.params?.id) {
            this.getGrind(this.props.match.params.id);
        }
    }


    /**
     * Retrieve the existing grind session if we are editing
     *
     * @param grindId Unique identifier of the grind session
     */
    getGrind(grindId) {
        GrindDataService.get(grindId)
            .then(response => {

                let grind = response.data;

                // Check auth
                if (AuthService.getCurrentUser().roles.indexOf(["ROLE_ADMIN"])) {
                    this.setState({
                        authorized: true,
                    })
                } else if (grind.user.id !== AuthService.getCurrentUser().id) {
                    this.setState({
                        authorized: false,
                    })
                } else {
                    this.setState({
                        authorized: true,
                    })
                }

                let character = grind.c_character;
                let grindspot = grind.grindspot;
                let droppedItems = [];

                for (let oldItemDropped in grind.grindItems) {
                    // Set price and qty
                    let tempItem = grind.grindItems[oldItemDropped].item;
                    tempItem.quantity = grind.grindItems[oldItemDropped].quantity;
                    tempItem.price = grind.grindItems[oldItemDropped].price;

                    // Add to the dropped item array refacto
                    droppedItems.push(tempItem);
                }

                // Fill the quantites and prices of previously added items
                for (let itemGrindspot in grindspot.items) {
                    if (droppedItems.indexOf(grindspot.items[itemGrindspot])) {
                        for (let itemDropped in droppedItems) {
                            if (grindspot.items[itemGrindspot].id === droppedItems[itemDropped].id) {
                                grindspot.items[itemGrindspot].quantity = droppedItems[itemDropped].quantity;
                                grindspot.items[itemGrindspot].price = droppedItems[itemDropped].price;
                            }
                        }
                    }
                }

                // Alphabetical Order Sort
                grindspot.items.sort(function (a, b) {
                    if (a.name < b.name) {
                        return -1;
                    }
                    if (a.name > b.name) {
                        return 1;
                    }
                    return 0;
                });


                this.setState({
                    grind: grind,
                    grindspot: grindspot,
                    character: character,
                    timerTime: grind.duration,
                    valuePack: grind.valuePack,
                    kamasylve: grind.kamasylve,
                    blueScroll: grind.blueScroll,
                    yellowScroll: grind.yellowScroll,
                    tent: grind.tent,
                    agris: grind.agris,
                    dropEvent: grind.dropEvent,
                    node: grind.node,
                });
            })
            .catch(e => {
                console.log(e);
            });
    }


    /**
     * Reset states to default if we create a new one after the one just submitted
     */
    newGrind() {
        this.setState({
            id: null,

            // init grind options
            valuePack: false,
            kamasylve: false,
            blueScroll: false,
            yellowScroll: false,
            tent: false,
            agris: false,
            dropEvent: false,
            node: 1,

            authorized: true,

            character: {},

            characters: [],

            grindspotsList: [],

            grindspot: {},

            submitted: false,

            // Timer states
            timerOn: false,
            timerStart: 0,
            timerTime: 3600000,
        });

        // Check auth
        this.checkAuth();

        // Retrieve all Grindspots
        this.retrieveGrindspots();

        // Retrieve all Characters for current User
        this.retrieveCharacters();
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getUserBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    /**
     * Adjusts the timer time + / -
     * @param input input to edit (hours, minutes, ...)
     */
    adjustTimer = input => {
        const {timerTime, timerOn} = this.state;
        const max = 216000000;
        if (!timerOn) {
            if (input === "incHours" && timerTime + 3600000 < max) {
                this.setState({timerTime: timerTime + 3600000});
            } else if (input === "decHours" && timerTime - 3600000 >= 0) {
                this.setState({timerTime: timerTime - 3600000});
            } else if (input === "incMinutes" && timerTime + 60000 < max) {
                this.setState({timerTime: timerTime + 60000});
            } else if (input === "decMinutes" && timerTime - 60000 >= 0) {
                this.setState({timerTime: timerTime - 60000});
            } else if (input === "incSeconds" && timerTime + 1000 < max) {
                this.setState({timerTime: timerTime + 1000});
            } else if (input === "decSeconds" && timerTime - 1000 >= 0) {
                this.setState({timerTime: timerTime - 1000});
            }
        }
    };

    /**
     * This function will be called when the timer is started or resumed
     */
    startTimer = () => {
        this.setState({
            timerOn: true,
            timerTime: this.state.timerTime,
            // timerStart will set our start time either to when the timer was started, or what that time would have been if the timer is resumed.
            timerStart: Date.now() - this.state.timerTime
        });
        this.timer = setInterval(() => {
            this.setState({
                timerTime: Date.now() - this.state.timerStart
            });
        }, 10);
    };

    /**
     * Stops the timer
     */
    stopTimer = () => {
        this.setState({timerOn: false});
        clearInterval(this.timer);
    };

    setGrindRedirect = (path) => {
        this.setState({grindRedirect: path});
    };

    /**
     * Resets the timer
     */
    resetTimer = () => {
        this.setState({
            timerStart: 0,
            timerTime: 0
        });
    };

    /**
     * Inverts value pack value
     */
    changeValuePack() {
        this.setState({valuePack: !this.state.valuePack});
    }

    /**
     * Inverts kamasylve value
     */
    changeKamasylve() {
        this.setState({kamasylve: !this.state.kamasylve});
    }

    /**
     * Inverts blueScroll value
     */
    changeBlueScroll() {
        this.setState({blueScroll: !this.state.blueScroll});
    }

    /**
     * Inverts blueScroll value
     */
    changeYellowScroll() {
        this.setState({yellowScroll: !this.state.yellowScroll});
    }

    /**
     * Inverts dropEvent value
     */
    changeDropEvent() {
        this.setState({dropEvent: !this.state.dropEvent});
    }

    /**
     * Inverts tent value
     */
    changeTent() {
        this.setState({tent: !this.state.tent});
    }

    /**
     * Inverts agris value
     */
    changeAgris() {
        this.setState({agris: !this.state.agris});
    }

    /**
     * Set node value
     */
    onChangeNode = (e) => {
        if ((e.target.value > 0 && e.target.value < 11) || e.target.value === "") {
            this.setState({node: e.target.value});
        }
    }


    /**
     * Retrieve all grindspots
     */
    retrieveGrindspots() {
        GrindspotDataService.getAll()
            .then(response => {
                this.setState({
                    grindspotsList: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }


    /**
     * Retrieve all characters of the user
     */
    retrieveCharacters() {
        let user = AuthService.getCurrentUser();

        CharacterDataService.getByUser(user.username)
            .then(response => {
                this.setState({
                    characters: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Change the selected grindspot
     *
     * @param e
     */
    onChangeGrindspot = (e) => {
        GrindspotDataService.findById(e.target.value)
            .then(response => {
                this.setState({
                    grindspot: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Change the selected Character
     *
     * @param e
     */
    onChangeCharacter = (e) => {
        CharacterDataService.findById(e.target.value)
            .then(response => {
                this.setState({
                    character: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Changes item quantity and refreshes the items selected state
     *
     * @param index index of the item to change the quantity
     * @param e
     */
    onChangeItemQuantity = (index, e) => {
        // SET THE ITEM QUANTITY
        let newitem = this.state.grindspot.items[index];
        let grindspot = this.state.grindspot;

        newitem.quantity = parseInt(e.target.value);
        grindspot.items[index] = newitem;

        this.setState({
            // Adds modified item to the selected items list
            grindspot: grindspot
        });
    }

    /**
     * Changes item price and refreshes the items selected state
     *
     * @param index index of the item to change the price
     * @param e
     */
    onChangeItemPrice = (index, e) => {
        // SET THE ITEM PRICE
        let newitem = this.state.grindspot.items[index];
        let grindspot = this.state.grindspot;

        newitem.price = parseInt(e.target.value);
        grindspot.items[index] = newitem;

        this.setState({
            // Adds modified item to the selected items list
            items: grindspot
        });
    }

    /**
     * Create a new grind session
     */
    submitGrind() {
        // We retrieve the user without its roles to avoid issues with the parsing
        let user = AuthService.getCurrentUser();
        user.roles = null;

        // Format and create a GrindItem Object
        let grindItems = [];
        for (var item in this.state.grindspot.items) {
            // Avoid saving items with no quantity or negative values
            if (this.state.grindspot.items[item].quantity > 0) {
                // Add the item to the GrindItems array
                grindItems.push({
                    item: this.state.grindspot.items[item],
                    quantity: this.state.grindspot.items[item].quantity,
                    price: this.state.grindspot.items[item].price
                })
            }
        }

        // Check node level for missinputs
        if (this.state.node === "") {
            this.setState({node: 1});
        }

        let data = {
            grindspot: this.state.grindspot,
            grindItems: grindItems,
            duration: this.state.timerTime,
            user: user,
            c_character: this.state.character,
            valuePack: this.state.valuePack,
            kamasylve: this.state.kamasylve,
            dropEvent: this.state.dropEvent,
            blueScroll: this.state.blueScroll,
            yellowScroll: this.state.yellowScroll,
            agris: this.state.agris,
            tent: this.state.tent,
            node: this.state.node,
        };

        GrindDataService.create(data)
            .then(response => {
                this.setState({
                    submitted: true
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Save the modifications to the existing grind session
     */
    saveGrind() {
        // We retrieve the user without its roles to avoid issues with the parsing
        let user = AuthService.getCurrentUser();
        user.roles = null;

        // Format and create a GrindItem Object
        let grindItems = [];
        for (var item in this.state.grindspot.items) {
            // Avoid saving items with no quantity or negative values
            if (this.state.grindspot.items[item].quantity >= 0) {
                // Add the item to the GrindItems array
                grindItems.push({
                    item: this.state.grindspot.items[item],
                    quantity: this.state.grindspot.items[item].quantity,
                    price: this.state.grindspot.items[item].price
                })
            }
        }

        // Check node level for missinputs
        if (this.state.node === "") {
            this.setState({node: 1});
        }

        let data = {
            grindspot: this.state.grindspot,
            grindItems: grindItems,
            duration: this.state.timerTime,
            user: user,
            c_character: this.state.character,
            valuePack: this.state.valuePack,
            kamasylve: this.state.kamasylve,
            dropEvent: this.state.dropEvent,
            blueScroll: this.state.blueScroll,
            yellowScroll: this.state.yellowScroll,
            agris: this.state.agris,
            tent: this.state.tent,
            node: this.state.node,
        };
        console.log(data);

        GrindDataService.update(this.state.grind.id, data)
            .then(response => {
                this.setState({
                    submitted: true
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Redirect to profile
     */
    redirectProfile() {
        this.setState({
            redirectProfile: true,
        });
    }

    render() {
        const {grindspot} = this.state;
        return (
            <div>
                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}

                {this.state.grindRedirect && (
                    <div>
                        <Redirect to={{
                            pathname: this.state.grindRedirect
                        }}/>
                    </div>
                )}


                {this.state.redirectProfile && (
                    <Redirect to='/profile'/>
                )}

                <header className="page-header">
                    <h3>
                        <div className={"page-header-flex"}>
                            <div className={"page-header-flex-title"}>Grind Session View</div>


                            <div className={"page-header-flex-icon"}>
                                <i className="bi bi-pencil-fill text-warning"
                                   onClick={() => this.setGrindRedirect("/grinds/edit/" + this.state.grind.id)}
                                />
                            </div>
                        </div>
                    </h3>
                </header>


                <div>
                    {this.state.submitted ? (
                        <div className={"block block-submit"}>
                            <h4>You submitted successfully!</h4>
                            <button className="btn-submit-return" onClick={this.newGrind}>
                                Add another
                            </button>
                            <button className="btn-submit-return" onClick={this.redirectProfile}>
                                Back to profile
                            </button>
                        </div>
                    ) : (
                        <div>
                            {/* Character section */}

                            {this.state.characters ? (
                                <div>
                                    <div className={"block block-character block-character-grindspot"}>
                                        <h4>Character</h4>

                                        <div>
                                            <label htmlFor="name">Name</label>
                                            <select disabled={"disabled"}
                                                    className={"input-outlined input-outlined-disabled"}
                                                    onChange={this.onChangeCharacter}
                                                    value={this.state.character?.id}>
                                                <option disabled selected value> -- Select a Character --</option>
                                                {this.state.characters &&
                                                this.state.characters.map((character, index) => (
                                                    <option key={index} value={character.id}>{character.name}</option>
                                                ))}
                                            </select>
                                        </div>


                                        <div>
                                            <label htmlFor="classe">Class</label>
                                            <select disabled={"disabled"}
                                                    className={"input-outlined input-outlined-disabled"} id="classe"
                                                    name={"classe"}>
                                                <option disabled selected
                                                        value={this.state.character?.classe?.id}>{this.state.character?.classe?.name}</option>
                                            </select>
                                        </div>


                                        <div>
                                            <label htmlFor="kutum">Sub-Weapon</label>
                                            <select disabled={"disabled"}
                                                    className={"input-outlined input-outlined-disabled"} id="kutum"
                                                    name={"kutum"}
                                                    value={this.state.character?.kutum}
                                            >
                                                <option disabled selected value> -- Select a Character --</option>
                                                <option value={0}>Nouver</option>
                                                <option value={1}>TET Kutum</option>
                                                <option value={2}>PEN Kutum</option>
                                            </select>
                                        </div>

                                        <div>
                                            <label htmlFor="level">Level</label>
                                            <input
                                                type="number"
                                                className="input-outlined input-outlined-disabled text-center"
                                                min={"0"}
                                                max={"99"}
                                                id="level"
                                                required
                                                disabled={"disabled"}
                                                value={this.state.character?.level}
                                                name="level"
                                            />
                                        </div>

                                        <div className={"grid-container-3"}>
                                            <div className={"col1"}>
                                                <label htmlFor="ap">AP</label>
                                                <input
                                                    type="number"
                                                    className="input-outlined input-outlined-disabled text-center"
                                                    id="ap"
                                                    min={"0"}
                                                    max={"999"}
                                                    required
                                                    value={this.state.character?.ap}
                                                    disabled={"disabled"}
                                                    name="ap"
                                                />
                                            </div>
                                            <div className={"col1"}>
                                                <label htmlFor="aap">AAP</label>
                                                <input
                                                    type="number"
                                                    className="input-outlined input-outlined-disabled text-center"
                                                    id="aap"
                                                    min={"0"}
                                                    max={"999"}
                                                    required
                                                    value={this.state.character?.aap}
                                                    disabled={"disabled"}
                                                    name="aap"
                                                />
                                            </div>
                                            <div className={"col1"}>
                                                <label htmlFor="dp">DP</label>
                                                <input
                                                    type="number"
                                                    className="input-outlined input-outlined-disabled text-center"
                                                    id="dp"
                                                    min={"0"}
                                                    max={"999"}
                                                    required
                                                    value={this.state.character?.dp}
                                                    disabled={"disabled"}
                                                    name="dp"
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    {this.state.character.id &&
                                    <>
                                        <div className={"block block-grindspot"}>
                                            <h4>Grindspots</h4>
                                            <div className={"grid-container-2 text-center"}>
                                                <div>
                                                    <label>Name</label>
                                                    {this.state.grindspot?.name}
                                                </div>
                                                <div>
                                                    <label htmlFor="">Recommended AP</label>
                                                    <span>{this.state.grindspot.recommendedAP}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className={"block block-stopwatch-grindspot grid-container-2"}>
                                            <div>
                                                <Stopwatch startTimer={this.startTimer} stopTimer={this.stopTimer}
                                                           resetTimer={this.resetTimer} adjustTimer={this.adjustTimer}
                                                           timerOn={this.state.timerOn}
                                                           timerTime={this.state.timerTime}
                                                           noEdit={true}/>
                                            </div>
                                            <div>
                                                <Countdown/>
                                            </div>
                                        </div>


                                        {this.state.grindspot.id &&
                                        <>
                                            <div className={"block block-options-grindspot"}>
                                                <h4>Options</h4>

                                                <div className={"flex-options"}>
                                                    {/* Value pack Switch */}
                                                    <div className={"flex-option-container"}>
                                                        <div className={"corrupted-icon-container"}>
                                                            <div>
                                                                {this.state.valuePack ? (
                                                                    <img className={"corrupted-icon"}
                                                                         alt={"Value pack icon"}
                                                                         src={valuePack}/>
                                                                ) : (
                                                                    <img className={"greyscale corrupted-icon"}
                                                                         alt={"Value pack icon"}
                                                                         src={valuePack}/>
                                                                )}
                                                            </div>
                                                        </div>
                                                        <div className={"increase-descrease"}>
                                                            <Switch disabled={"disabled"} activeBoxShadow={"null"}
                                                                    offColor={"#403F51"}
                                                                    onColor={"#8D00F2"}
                                                                    onChange={this.changeValuePack}
                                                                    checked={this.state.valuePack}/>
                                                        </div>
                                                    </div>


                                                    {/* Kamasylve Switch */}
                                                    <div className={"flex-option-container"}>
                                                        <div className={"corrupted-icon-container"}>
                                                            <div>
                                                                {this.state.kamasylve ? (
                                                                    <img className={"corrupted-icon"}
                                                                         alt={"Kamasylve icon"}
                                                                         src={kamasylve}/>
                                                                ) : (
                                                                    <img className={"greyscale corrupted-icon"}
                                                                         alt={"Kamasylve icon"}
                                                                         src={kamasylve}/>
                                                                )}
                                                            </div>
                                                        </div>
                                                        <div className={"increase-descrease"}>
                                                            <Switch disabled={"disabled"} activeBoxShadow={"null"}
                                                                    offColor={"#403F51"}
                                                                    onColor={"#8D00F2"}
                                                                    onChange={this.changeKamasylve}
                                                                    checked={this.state.kamasylve}/>
                                                        </div>
                                                    </div>


                                                    {/* Drop Event Switch */}
                                                    <div className={"flex-option-container"}>
                                                        <div className={"corrupted-icon-container"}>
                                                            <div>
                                                                {this.state.dropEvent ? (
                                                                    <img className={"corrupted-icon"}
                                                                         alt={"Drop event icon"}
                                                                         src={dropEvent}/>
                                                                ) : (
                                                                    <img className={"greyscale corrupted-icon"}
                                                                         alt={"Drop event icon"}
                                                                         src={dropEvent}/>
                                                                )}
                                                            </div>
                                                        </div>
                                                        <div className={"increase-descrease"}>
                                                            <Switch disabled={"disabled"} activeBoxShadow={"null"}
                                                                    offColor={"#403F51"}
                                                                    onColor={"#8D00F2"}
                                                                    onChange={this.changeDropEvent}
                                                                    checked={this.state.dropEvent}/>
                                                        </div>
                                                    </div>


                                                    {/* Blue 50% Lootscroll Switch */}
                                                    <div className={"flex-option-container"}>
                                                        <div className={"corrupted-icon-container"}>
                                                            <div>
                                                                {this.state.blueScroll ? (
                                                                    <img className={"corrupted-icon"}
                                                                         alt={"Blue LS icon"}
                                                                         src={blueScroll}/>
                                                                ) : (
                                                                    <img className={"greyscale corrupted-icon"}
                                                                         alt={"Blue LS icon"}
                                                                         src={blueScroll}/>
                                                                )}
                                                            </div>
                                                        </div>
                                                        <div className={"increase-descrease"}>
                                                            <Switch disabled={"disabled"} activeBoxShadow={"null"}
                                                                    offColor={"#403F51"}
                                                                    onColor={"#8D00F2"}
                                                                    onChange={this.changeBlueScroll}
                                                                    checked={this.state.blueScroll}/>
                                                        </div>
                                                    </div>


                                                    {/* Yellow 100% Lootscroll Switch */}
                                                    <div className={"flex-option-container"}>
                                                        <div className={"corrupted-icon-container"}>
                                                            <div>
                                                                {this.state.yellowScroll ? (
                                                                    <img className={"corrupted-icon"}
                                                                         alt={"Yellow LS icon"}
                                                                         src={yellowScroll}/>
                                                                ) : (
                                                                    <img className={"greyscale corrupted-icon"}
                                                                         alt={"Yellow LS icon"}
                                                                         src={yellowScroll}/>
                                                                )}
                                                            </div>
                                                        </div>
                                                        <div className={"increase-descrease"}>
                                                            <Switch disabled={"disabled"} activeBoxShadow={"null"}
                                                                    offColor={"#403F51"}
                                                                    onColor={"#8D00F2"}
                                                                    onChange={this.changeYellowScroll}
                                                                    checked={this.state.yellowScroll}/>
                                                        </div>
                                                    </div>

                                                    {/* Agris Fever Switch */}
                                                    <div className={"flex-option-container"}>
                                                        <div className={"corrupted-icon-container"}>
                                                            <div>
                                                                {this.state.agris ? (
                                                                    <img className={"corrupted-icon"}
                                                                         alt={"Agris Fever icon"}
                                                                         src={agris}/>
                                                                ) : (
                                                                    <img className={"greyscale corrupted-icon"}
                                                                         alt={"Agris Fever icon"}
                                                                         src={agris}/>
                                                                )}
                                                            </div>
                                                        </div>
                                                        <div className={"increase-descrease"}>
                                                            <Switch disabled={"disabled"} activeBoxShadow={"null"}
                                                                    offColor={"#403F51"}
                                                                    onColor={"#8D00F2"}
                                                                    onChange={this.changeAgris}
                                                                    checked={this.state.agris}/>
                                                        </div>
                                                    </div>

                                                    {/* Tent Droprate buff 50% */}
                                                    <div>
                                                        <div className={"corrupted-icon-container"}>
                                                            <div>
                                                                {this.state.tent ? (
                                                                    <img className={"corrupted-icon"}
                                                                         alt={"Tent buff icon"}
                                                                         src={tent}/>
                                                                ) : (
                                                                    <img className={"greyscale corrupted-icon"}
                                                                         alt={"Tent buff icon"}
                                                                         src={tent}/>
                                                                )}
                                                            </div>
                                                        </div>
                                                        <div className={"increase-descrease"}>
                                                            <Switch disabled={"disabled"} activeBoxShadow={"null"}
                                                                    offColor={"#403F51"}
                                                                    onColor={"#8D00F2"}
                                                                    onChange={this.changeTent}
                                                                    checked={this.state.tent}/>
                                                        </div>
                                                    </div>


                                                    {/* Node level */}
                                                    <div>
                                                        <div className={"corrupted-icon-container"}>
                                                            <div>
                                                                <img className={"corrupted-icon"}
                                                                     alt={"Node Level icon"}
                                                                     src={node}/>
                                                            </div>
                                                        </div>

                                                        <div className={"increase-descrease increase-descrease-node"}>
                                                            <button
                                                                className={"button-circle button-circle--small button-hidden"}>
                                                                <i className="bi bi-dash"/>
                                                            </button>

                                                            <input id={"node"}
                                                                   disabled={"disabled"}
                                                                   className={"input-outlined input-outlined-disabled input-outlined-price node-input increase-descrease text-center"}
                                                                   name={"node"} type={"number"} min={"1"}
                                                                   max={"10"}
                                                                   value={this.state.node}
                                                                   onChange={this.onChangeNode}/>

                                                            <button
                                                                className={"button-circle button-circle--small button-hidden"}>
                                                                <i className="bi bi-plus"/>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="block">
                                                <h4>Available Items</h4>
                                                <table
                                                    className={"table table-bordered table-striped table-dark table-hover-purple"}>
                                                    <thead className={"thead-colored"}>
                                                    <tr>
                                                        <th>Item</th>
                                                        <th>Quantity</th>
                                                        <th>Price</th>
                                                        <th>Total</th>
                                                        <th>After Tax</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {grindspot.items &&
                                                    grindspot.items.map((item, index) => (
                                                        <tr key={item.id}>
                                                            <td>
                                                                {item.icon &&
                                                                <img alt="Item icon"
                                                                     className={"item-icon-table"}
                                                                     src={"data:image/jpeg;base64," + item.icon}/>
                                                                }
                                                                {item.name}
                                                            </td>
                                                            <td> {/*  Form quantity  */}
                                                                {item.quantity > 0 ? (
                                                                    <p>{item.quantity}</p>
                                                                ) : (
                                                                    <div>
                                                                        <p>0</p>
                                                                    </div>
                                                                )}

                                                            </td>
                                                            <td>
                                                                {/* Form price sold */}
                                                                {item.price > 0 ? (
                                                                    <p>{item.price.toLocaleString()}</p>
                                                                ) : (
                                                                    <p>0</p>
                                                                )}
                                                            </td>
                                                            <td>
                                                                {/* Form price sold */}
                                                                {item.quantity > 0 ? (
                                                                    <p>{(item.price * item.quantity).toLocaleString()}</p>
                                                                ) : (
                                                                    <p>0</p>
                                                                )}
                                                            </td>

                                                            <td>
                                                                {/* Form price sold */}
                                                                {item.taxed ? (
                                                                    <>
                                                                        {item.quantity > 0 ? (
                                                                            <>
                                                                                {this.state.grind.valuePack ? (
                                                                                    <p>{Math.round(item.price * item.quantity * 0.845).toLocaleString()}</p>
                                                                                ) : (
                                                                                    <p>{Math.round(item.price * item.quantity * 0.65).toLocaleString()}</p>
                                                                                )}
                                                                            </>
                                                                        ) : (
                                                                            <p>0</p>
                                                                        )}
                                                                    </>
                                                                ) : (
                                                                    <>
                                                                        {item.quantity > 0 ? (
                                                                            <p>{(item.price * item.quantity).toLocaleString()}</p>
                                                                        ) : (
                                                                            <p>0</p>
                                                                        )}
                                                                    </>
                                                                )}
                                                            </td>
                                                        </tr>
                                                    ))}
                                                    </tbody>
                                                </table>

                                                <div className={"block total-silver"}>
                                                    <div className={"grid-container-2"}>
                                                        <div className={"col1 final-profit"}>
                                                            Silver Gained
                                                        </div>
                                                        <div className={"col1 total-profit container-price"}>
                                                            <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                                            <div> {Math.round(this.state.grind.totalSilver).toLocaleString()}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </>
                                        }
                                    </>
                                    }
                                </div>
                            ) : (
                                <div>
                                    {/* Redirect to addCharacter component if the user doesn't have any character */}
                                    <Redirect to={{
                                        pathname: '/addCharacter',
                                        state: {reason: "You don't have any characters currently please create one before creating a grind session"}
                                    }}/>
                                </div>
                            )}

                        </div>
                    )}
                </div>
            </div>
        );
    }
}
