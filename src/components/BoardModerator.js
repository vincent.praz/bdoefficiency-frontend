import React, { useState, useEffect } from "react";

import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";

/**
 * Moderator Private Component (Unused as of now)
 * @returns {JSX.Element}
 * @constructor
 */
const BoardUser = () => {
    const [error, setError] = useState("");

    useEffect(() => {
        UserService.getModeratorBoard().then(
            (response) => {

            },
            (error) => {
                setError(error);
            }
        );
    }, []);

    // Redirect to login page if someone tries to manipulate the url to access the page without the proper access rights
    if (error){
        return <Redirect to='/unauthorized'  />
    }

    return (
        <div>
            <header className="jumbotron">
                <h3>Moderator Board</h3>
            </header>
        </div>
    );
};

export default BoardUser;
