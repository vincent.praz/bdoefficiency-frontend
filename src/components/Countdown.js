import React, {Component} from "react";
import elixir from '../assets/voices/elixir.mp3'

/**
 * Countdown
 * Typical countdown
 */
class Countdown extends Component {
    state = {
        timerOn: false,
        timerStart: 900000,
        timerTime: 900000
    };


    startTimer = () => {
        this.setState({
            timerOn: true,
            timerTime: this.state.timerTime,
            timerStart: this.state.timerTime
        });
        this.timer = setInterval(() => {
            const newTime = this.state.timerTime - 10;
            if (newTime >= 0) {
                this.setState({
                    timerTime: newTime
                });
            } else {
                // Ending of the countdown
                // Plays audio when timer ended
                const audioEl = document.getElementsByClassName("audio-element")[0];
                audioEl.play();

                // Restart countdown to initial value
                this.setState({
                    timerTime: this.state.timerStart
                });

                // this avoid getting stuck without beeing able to stop the countdown
                if (this.state.timerStart <= 1000) {
                    this.stopTimer();
                }
            }
        }, 10);
    };

    stopTimer = () => {
        clearInterval(this.timer);
        this.setState({timerOn: false});
    };
    resetTimer = () => {
        if (this.state.timerOn === false) {
            this.setState({
                timerTime: this.state.timerStart
            });
        }
    };

    /**
     * Handles the states of the buttons that adjust the timer
     *
     * @param input
     */
    adjustTimer = input => {
        const {timerTime, timerOn} = this.state;
        const max = 216000000;
        if (!timerOn) {
            if (input === "incHours" && timerTime + 3600000 < max) {
                this.setState({timerTime: timerTime + 3600000});
            } else if (input === "decHours" && timerTime - 3600000 >= 0) {
                this.setState({timerTime: timerTime - 3600000});
            } else if (input === "incMinutes" && timerTime + 60000 < max) {
                this.setState({timerTime: timerTime + 60000});
            } else if (input === "decMinutes" && timerTime - 60000 >= 0) {
                this.setState({timerTime: timerTime - 60000});
            } else if (input === "incSeconds" && timerTime + 1000 < max) {
                this.setState({timerTime: timerTime + 1000});
            } else if (input === "decSeconds" && timerTime - 1000 >= 0) {
                this.setState({timerTime: timerTime - 1000});
            }
        }
    };


    render() {
        /**
         * Formatting
         */
        const {timerTime, timerStart, timerOn} = this.state;
        let seconds = ("0" + (Math.floor((timerTime / 1000) % 60) % 60)).slice(-2);
        let minutes = ("0" + Math.floor((timerTime / 60000) % 60)).slice(-2);
        let hours = ("0" + Math.floor((timerTime / 3600000) % 60)).slice(-2);
        return (
            <div className="Countdown">
                <div className="Countdown-header"><h4>Countdown</h4></div>
                <div className="Countdown-label">Hours : Minutes : Seconds</div>
                <div className="Countdown-display">
                    <button onClick={() => this.adjustTimer("incHours")}>
                        <i className="bi bi-arrow-up"/></button>
                    <button onClick={() => this.adjustTimer("incMinutes")}>
                        <i className="bi bi-arrow-up"/></button>
                    <button onClick={() => this.adjustTimer("incSeconds")}>
                        <i className="bi bi-arrow-up"/></button>
                    <div className="Countdown-time">
                        {hours} : {minutes} : {seconds}
                    </div>
                    <button onClick={() => this.adjustTimer("decHours")}>
                        <i className="bi bi-arrow-down"/>
                    </button>
                    <button onClick={() => this.adjustTimer("decMinutes")}>
                        <i className="bi bi-arrow-down"/>
                    </button>
                    <button onClick={() => this.adjustTimer("decSeconds")}>
                        <i className="bi bi-arrow-down"/>
                    </button>
                </div>
                <div className={"countdown-buttons"}>
                    {timerOn === false &&
                    (timerStart === 0 || timerTime === timerStart) && (
                        <button onClick={this.startTimer}>Start</button>
                    )}
                    {timerOn === true && timerTime >= 1000 && (
                        <button onClick={this.stopTimer}>Stop</button>
                    )}
                    {timerOn === false &&
                    (timerStart !== 0 && timerStart !== timerTime && timerTime !== 0) && (
                        <button onClick={this.startTimer}>Resume</button>
                    )}
                    {(timerOn === false || timerTime < 1000) &&
                    (timerStart !== timerTime && timerStart > 0) && (
                        <button onClick={this.resetTimer}>Reset</button>
                    )}
                </div>

                <audio className="audio-element">
                    <source src={elixir}/>
                </audio>
            </div>
        );
    }
}

export default Countdown;
