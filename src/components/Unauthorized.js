import React from "react";
import unauthorizedIcon from '../assets/images/icons/401.png'

/**
 * This component is displayed when you access an unauthorized resource
 *
 * @returns {JSX.Element}
 * @constructor
 */
const Unauthorized = () => {

    return (
        <div>
            <header className="page-header">
                <h3>You are not authorized to view this content</h3>
                <img alt="Unauthorized icon" src={unauthorizedIcon}/>
            </header>
        </div>
    );
};

export default Unauthorized;
