import React, {Component} from "react";
import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";
import ItemDataService from "../services/item.service";
import Stopwatch from "./Stopwatch";
import valuePack from "../assets/images/icons/value-pack.png";
import Switch from "react-switch";
import silver from "../assets/images/icons/silver.png";

/**
 * Scroll calculator
 * This calculator allows the user to calculate how many scroll parts he will have to buy,
 * how much money it represents as investments and how much benefits he could be rewarded with
 * depending of the execution time of 1 scroll
 */
export default class ScrollCalculator extends Component {
    constructor(props) {
        super(props);

        this.changeValuePack = this.changeValuePack.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.startTimer = this.startTimer.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
        this.adjustTimer = this.adjustTimer.bind(this);
        this.retrieveItems = this.retrieveItems.bind(this);
        this.chooseScroll = this.chooseScroll.bind(this);
        this.onChangeScrollPrice = this.onChangeScrollPrice.bind(this);
        this.onChangeScrollQuantity = this.onChangeScrollQuantity.bind(this);
        this.calculateInvestment = this.calculateInvestment.bind(this);
        this.onChangeMemoryFragmentPrice = this.onChangeMemoryFragmentPrice.bind(this);
        this.onChangeMemoryFragmentQuantity = this.onChangeMemoryFragmentQuantity.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);


        this.state = {
            id: null,

            // Items needed
            memoryFragment: {},
            pilaScroll: {},
            relicShard: {},
            forbiddenBook: {},

            // Calculated values
            nbrScroll: 0,
            investments: 0,
            benefits: 0,
            memoryFragQty: 0,

            selectedScroll: {},

            valuePack: false,

            authorized: true,

            submitted: false,

            // Timer states
            timerOn: false,
            timerStart: 0,
            timerTime: 0,
        };
    }

    componentDidMount() {
        // Check auth
        this.checkAuth();

        // Retrieve items infos
        this.retrieveItems();
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getUserBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    /**
     * Invert Value pack boolean value
     */
    changeValuePack() {
        this.setState({
            valuePack: !this.state.valuePack
            // Recalculate Benefit
        }, () => this.calculateBenefit(this.state.investments, this.state.memoryFragQty));
    }

    /**
     * Choose the scroll and sets it to selectedScroll state
     */
    chooseScroll(e) {
        let selectedScroll = {};
        this.stopTimer();

        switch (e.target.name) {
            case this.state.pilaScroll.name:
                selectedScroll = this.state.pilaScroll;
                break;
            case this.state.relicShard.name:
                selectedScroll = this.state.relicShard;
                break;
            case this.state.forbiddenBook.name:
                selectedScroll = this.state.forbiddenBook;
                break;
            default:
                break;
        }

        // We set quantity in case the timer is selected already
        selectedScroll.quantity = this.state.nbrScroll * 5;

        this.setState({
            selectedScroll: selectedScroll,
            timerTime: 0,
            timerStart: 0,
            timerOn: false,
        });

        // Calculate total price invested
        this.calculateInvestment(selectedScroll);
    }

    /**
     * Calculates the number of scrolls that you can do in an hour
     * Calculates the number of Memfrags you get in an hour
     * @param timertime The time that you take to do 1 scroll
     */
    calculateNumberofScrollsInHour(timertime) {
        // We avoid dividing by 0
        if (timertime !== 0) {
            let nbrScroll = Math.round(3600000 / timertime);

            let selectedScroll = this.state.selectedScroll;
            selectedScroll.quantity = nbrScroll * 5;

            // Calculate total price invested
            let investment = selectedScroll.quantity * selectedScroll.price;
            // Calculate benefit
            let benefit = this.state.memoryFragment.price * selectedScroll.avgMem * nbrScroll;

            if (this.state.valuePack) {
                benefit = benefit * 0.845;
            } else {
                benefit = benefit * 0.65;
            }

            benefit = benefit - investment;

            this.setState({
                nbrScroll: nbrScroll,
                selectedScroll: selectedScroll,
                investments: investment,
                benefits: Math.round(benefit),
                memoryFragQty: this.state.selectedScroll.avgMem * nbrScroll,// Calculate number of memory fragments dropped
            });
        } else {
            let selectedScroll = this.state.selectedScroll;
            selectedScroll.quantity = 0;

            this.calculateInvestment(selectedScroll);

            this.setState({
                nbrScroll: 0,
                selectedScroll: selectedScroll,
                memoryFragQty: 0,
            });
        }
    }

    onChangeMemoryFragmentPrice(e) {
        let memoryFragment = this.state.memoryFragment;

        let newValue = e.target.value;

        if (newValue < 0) {
            newValue = 0;
        }

        memoryFragment.price = newValue;

        this.setState({
            memoryFragment: memoryFragment,
        });

        // Recalculate Benefit
        this.calculateBenefit(this.state.investments, this.state.memoryFragQty);
    }

    onChangeMemoryFragmentQuantity(e) {
        let newValue = e.target.value;

        if (newValue < 0) {
            newValue = 0;
        }

        this.setState({
            memoryFragQty: newValue,
        });

        // Recalculate Benefit
        this.calculateBenefit(this.state.investments, e.target.value);
    }

    calculateBenefit(invest, qty) {
        let benefit = 0;

        if (this.state.valuePack) {
            benefit = this.state.memoryFragment.price * qty * 0.845 - invest;
        } else {
            benefit = this.state.memoryFragment.price * qty * 0.65 - invest;
        }

        this.setState({
            benefits: Math.round(benefit),
        });
    }

    onChangeScrollPrice(e) {
        let selectedScroll = this.state.selectedScroll;

        if (e.target.value >= 0) {
            selectedScroll.price = e.target.value;
        } else {
            selectedScroll.price = 0;
        }

        this.setState({
            selectedScroll: selectedScroll
        });


        // Recalculate Investment
        this.calculateInvestment(selectedScroll);
    }

    onChangeScrollQuantity(e) {
        let selectedScroll = this.state.selectedScroll;

        if (e.target.value >= 0) {
            selectedScroll.quantity = e.target.value;
        } else {
            selectedScroll.quantity = 0;
        }

        this.setState({
            selectedScroll: selectedScroll
        });


        // Recalculate Investment
        this.calculateInvestment(selectedScroll);
    }

    /**
     * Sets the investment for the Selected Scroll Quantity * Price
     * @param selectedScroll The scroll that we want to calculate the investment for
     */
    calculateInvestment(selectedScroll) {
        let investment = selectedScroll.quantity * selectedScroll.price;
        this.setState({
            investments: investment,
        });
        this.calculateBenefit(investment, this.state.selectedScroll.avgMem * this.state.nbrScroll);
    }

    /**
     * Calls the backend API to get the infos about the items
     */
    retrieveItems() {
        ItemDataService.findByName("Memory Fragment")
            .then(response => {
                this.setState({
                    memoryFragment: response.data[0]
                });
                ItemDataService.findByName("Scroll Written in Ancient Language")
                    .then(response => {

                        // Set default values for qty of memory fragments dropped
                        let pilaScroll = response.data[0];
                        pilaScroll.avgMem = 7;

                        this.setState({
                            pilaScroll: pilaScroll
                        });
                        ItemDataService.findByName("Ancient Relic Crystal Shard")
                            .then(response => {

                                // Set default values for qty of memory fragments dropped
                                let relicShard = response.data[0];
                                relicShard.avgMem = 5;

                                this.setState({
                                    relicShard: relicShard
                                });

                                ItemDataService.findByName("Forbidden Book")
                                    .then(response => {

                                        // Set default values for qty of memory fragments dropped
                                        let forbiddenBook = response.data[0];
                                        forbiddenBook.avgMem = 6;

                                        this.setState({
                                            forbiddenBook: forbiddenBook
                                        });
                                    })
                                    .catch(e => {
                                        console.log(e);
                                    });
                            })
                            .catch(e => {
                                console.log(e);
                            });
                    })
                    .catch(e => {
                        console.log(e);
                    });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * This function will be called when the timer is started or resumed
     */
    startTimer = () => {
        this.setState({
            timerOn: true,
            timerTime: this.state.timerTime,
            // timerStart will set our start time either to when the timer was started, or what that time would have been if the timer is resumed.
            timerStart: Date.now() - this.state.timerTime
        });
        this.timer = setInterval(() => {
            this.setState({
                timerTime: Date.now() - this.state.timerStart
            });
        }, 10);
    };

    /**
     * Stops the timer
     */
    stopTimer = () => {
        this.setState({timerOn: false});
        clearInterval(this.timer);

        // Calculate Number of Scrolls done in an hour
        this.calculateNumberofScrollsInHour(this.state.timerTime);
    };

    /**
     * Resets the timer
     */
    resetTimer = () => {
        this.setState({
            timerStart: 0,
            timerTime: 0
        });

        // Calculate Number of Scrolls done in an hour
        this.calculateNumberofScrollsInHour(0);
    };

    /**
     * Adjusts the timer time + / -
     * @param input input to edit (hours, minutes, ...)
     */
    adjustTimer = input => {
        const {timerTime, timerOn} = this.state;
        const max = 216000000;
        if (!timerOn) {
            if (input === "incHours" && timerTime + 3600000 < max) {
                this.setState({timerTime: timerTime + 3600000});
                // We have to call the function here because the state doesn't have the time to change before the function is called
                // Calculate Number of Scrolls done in an hour
                this.calculateNumberofScrollsInHour(timerTime + 3600000);
            } else if (input === "decHours" && timerTime - 3600000 >= 0) {
                this.setState({timerTime: timerTime - 3600000});
                // Calculate Number of Scrolls done in an hour
                this.calculateNumberofScrollsInHour(timerTime - 3600000);
            } else if (input === "incMinutes" && timerTime + 60000 < max) {
                this.setState({timerTime: timerTime + 60000});
                // Calculate Number of Scrolls done in an hour
                this.calculateNumberofScrollsInHour(timerTime + 60000);
            } else if (input === "decMinutes" && timerTime - 60000 >= 0) {
                this.setState({timerTime: timerTime - 60000});
                // Calculate Number of Scrolls done in an hour
                this.calculateNumberofScrollsInHour(timerTime - 60000);
            } else if (input === "incSeconds" && timerTime + 1000 < max) {
                this.setState({timerTime: timerTime + 1000});
                // Calculate Number of Scrolls done in an hour
                this.calculateNumberofScrollsInHour(timerTime + 1000);
            } else if (input === "decSeconds" && timerTime - 1000 >= 0) {
                this.setState({timerTime: timerTime - 1000});
                // Calculate Number of Scrolls done in an hour
                this.calculateNumberofScrollsInHour(timerTime - 1000);
            }
        }
    };


    render() {
        return (
            <div className={"page-scroll"}>
                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}
                <header className="page-header">
                    <h1>Scroll Calculator</h1>
                </header>

                <div className={"block"}>
                    <h4>Select your Scroll</h4>

                    <div className={"grid-container-3 grid-container-scroll"}>
                        <div className={"corrupted-icon-container col1"}>
                            <div>
                                {this.state.selectedScroll.name === this.state.pilaScroll.name ? (
                                    <img className={"corrupted-icon cursor-pointer"}
                                         alt={"Pila Fe Scroll icon"}
                                         src={"data:image/jpeg;base64," + this.state.pilaScroll.icon}/>
                                ) : (
                                    <img className={"greyscale corrupted-icon cursor-pointer"}
                                         onClick={this.chooseScroll}
                                         name={this.state.pilaScroll.name}
                                         alt={"Pila Fe Scroll icon"}
                                         src={"data:image/jpeg;base64," + this.state.pilaScroll.icon}/>
                                )}
                            </div>
                        </div>


                        <div className={"corrupted-icon-container col1"}>
                            <div>
                                {this.state.selectedScroll.name === this.state.relicShard.name ? (
                                    <img className={"corrupted-icon cursor-pointer"}
                                         alt={"Relic Shard Scroll icon"}
                                         src={"data:image/jpeg;base64," + this.state.relicShard.icon}/>
                                ) : (
                                    <img className={"greyscale corrupted-icon cursor-pointer"}
                                         onClick={this.chooseScroll}
                                         name={this.state.relicShard.name}
                                         alt={"Relic Shard Scroll icon"}
                                         src={"data:image/jpeg;base64," + this.state.relicShard.icon}/>
                                )}
                            </div>
                        </div>

                        <div className={"corrupted-icon-container col1"}>
                            <div>
                                {this.state.selectedScroll.name === this.state.forbiddenBook.name ? (
                                    <img className={"corrupted-icon cursor-pointer"}
                                         alt={"Forbidden book icon"}
                                         src={"data:image/jpeg;base64," + this.state.forbiddenBook.icon}/>
                                ) : (
                                    <img className={"greyscale corrupted-icon cursor-pointer"}
                                         onClick={this.chooseScroll}
                                         name={this.state.forbiddenBook.name}
                                         alt={"Forbidden book icon"}
                                         src={"data:image/jpeg;base64," + this.state.forbiddenBook.icon}/>
                                )}
                            </div>
                        </div>
                    </div>
                </div>


                {this.state.selectedScroll.name && (
                    <div>
                        <div className={"block block-scroll no-background"}>
                            <h4>Time to complete</h4>

                            {/* Stopwatch to help the player to record his 1st run */}
                            <Stopwatch startTimer={this.startTimer} stopTimer={this.stopTimer}
                                       resetTimer={this.resetTimer} adjustTimer={this.adjustTimer}
                                       timerOn={this.state.timerOn} timerTime={this.state.timerTime}/>
                        </div>

                        {(!this.state.timerOn && this.state.timerTime > 0) && (
                            <div className={"grid-container-2"}>
                                <div className={"block investment-container-scroll"}>
                                    <h4>{this.state.nbrScroll} Scrolls / hour</h4>
                                    <div className={"corrupted-icon-container"}>
                                        <div>
                                            {this.state.selectedScroll.icon &&
                                            <img className={"corrupted-icon"} alt="Selected scroll icon"
                                                 src={"data:image/jpeg;base64," + this.state.selectedScroll.icon}/>
                                            }

                                            <span
                                                className={"quantity-fury"}>{this.state.selectedScroll.quantity}</span>
                                        </div>
                                    </div>

                                    <div className={"container-scroll-per-hour"}>
                                        <div className={"container-price needed-quantity"}>
                                            <label htmlFor={"quantity"}>Needed Quantity</label>
                                            <input type="number" id={"quantity"} name="quantity"
                                                   className={"input-outlined input-qty"}
                                                   value={this.state.selectedScroll.quantity}
                                                   min={"0"}
                                                   onChange={this.onChangeScrollQuantity}/>
                                        </div>

                                        <div className={"container-price needed-quantity"}>
                                            <label htmlFor={"price-fury"}>Price</label>
                                            <div className={"container-price container-price-page-scroll"}>
                                                <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                                <input
                                                    type="number"
                                                    className="input-outlined input-outlined-price"
                                                    id="price-fury"
                                                    required
                                                    value={this.state.selectedScroll.price}
                                                    onChange={this.onChangeScrollPrice}
                                                    name="price-fury"
                                                />
                                            </div>
                                        </div>
                                    </div>


                                    <div className={"block final-profit-scroll"}>
                                        <div className={"grid-container-2"}>
                                            <div className={"col1 final-profit"}>
                                                Investments
                                            </div>
                                            <div className={"col1 total-profit container-price"}>
                                                <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                                <div> {Math.round(this.state.investments).toLocaleString()}</div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div className={"block"}>
                                    <h4>Memory Fragments</h4>
                                    <div className={"corrupted-icon-container"}>
                                        <div>
                                            {this.state.memoryFragment &&
                                            <img className={"corrupted-icon"} alt="Selected scroll icon"
                                                 src={"data:image/jpeg;base64," + this.state.memoryFragment.icon}/>
                                            }

                                            <span
                                                className={"quantity-fury"}>{this.state.memoryFragQty}</span>
                                        </div>
                                    </div>

                                    <div className={"container-scroll-per-hour"}>
                                        <div className={"container-price needed-quantity"}>
                                            <label htmlFor={"quantity-frag"}>Quantity Dropped</label>
                                            <input type="number" id={"quantity-frag"} name="quantity-frag"
                                                   className={"input-outlined input-qty"}
                                                   value={this.state.memoryFragQty}
                                                   min={"0"}
                                                   onChange={this.onChangeMemoryFragmentQuantity}/>
                                        </div>

                                        <div className={"container-price needed-quantity"}>
                                            <label htmlFor={"quantity-frag"}>Price</label>
                                            <div className={"container-price container-price-page-scroll"}>
                                                <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                                <input type="number" id={"price-frag"} name="price-frag"
                                                       className={"input-outlined input-qty"}
                                                       value={this.state.memoryFragment.price}
                                                       min={"1"}
                                                       onChange={this.onChangeMemoryFragmentPrice}/>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 className={"title-result"}>Results</h4>
                                    <div className={"corrupted-icon-container valuepack-scroll-container"}>
                                        <div>
                                            {this.state.valuePack ? (
                                                <img className={"corrupted-icon"}
                                                     alt={"Value pack icon"}
                                                     src={valuePack}/>
                                            ) : (
                                                <img className={"greyscale corrupted-icon"}
                                                     alt={"Value pack icon"}
                                                     src={valuePack}/>
                                            )}
                                        </div>
                                    </div>
                                    <div className={"increase-descrease"}>
                                        <Switch activeBoxShadow={"null"}
                                                offColor={"#403F51"}
                                                onColor={"#8D00F2"}
                                                onChange={this.changeValuePack}
                                                checked={this.state.valuePack}/>
                                    </div>

                                    <div className={"block final-profit-scroll"}>
                                        <div className={"grid-container-2"}>

                                            {this.state.benefits >= 0 ? (
                                                <>
                                                    <div className={"col1 final-profit"}>
                                                        Profits
                                                    </div>
                                                    <div className={"col1 total-profit container-price"}>
                                                        <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                                        <div
                                                            className={"text-success"}> {Math.round(this.state.benefits).toLocaleString()}</div>
                                                    </div>
                                                </>
                                            ) : (
                                                <>
                                                    <div className={"col1 final-profit"}>
                                                        Losses
                                                    </div>
                                                    <div className={"col1 total-profit container-price"}>
                                                        <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                                        <div
                                                            className={"text-danger"}> {Math.round(this.state.benefits).toLocaleString()}</div>
                                                    </div>
                                                </>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        )}

                    </div>
                )}
            </div>
        );
    }
}
