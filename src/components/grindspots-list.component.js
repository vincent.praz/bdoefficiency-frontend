import React, {Component} from "react";
import GrindspotDataService from "../services/grindspot.service";
import {Redirect} from "react-router-dom";
import UserService from "../services/user.service";

/**
 * List of all the grindspots
 */
export default class GrindspotsList extends Component {
    constructor(props) {
        super(props);
        this.onChangeSearchName = this.onChangeSearchName.bind(this);
        this.retrieveGrindspots = this.retrieveGrindspots.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveGrindspot = this.setActiveGrindspot.bind(this);
        this.searchName = this.searchName.bind(this);
        this.checkAuth = this.checkAuth.bind(this);

        this.state = {
            grindspots: [],
            currentItem: null,
            currentIndex: -1,
            searchName: "",
            authorized: true,
        };
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getAdminBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    componentDidMount() {
        this.checkAuth();
        this.retrieveGrindspots();
    }

    /**
     * Handle the searchbar
     *
     * @param e
     */
    onChangeSearchName(e) {
        const searchName = e.target.value;

        this.setState({
            searchName: searchName
        });

        // Refresh the entire list if the user deletes his search completely
        if (searchName.length === 0) {
            this.refreshList();
        }

        // Execute the search every character typed
        this.searchName();
    }

    /**
     * Retrieve all grindspots
     */
    retrieveGrindspots() {
        GrindspotDataService.getAll()
            .then(response => {
                this.setState({
                    grindspots: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Refresh grindspots list
     */
    refreshList() {
        this.retrieveGrindspots();
        this.setState({
            currentGrindspot: null,
            currentIndex: -1
        });
    }

    /**
     * Set Active grindspot
     * @param item Grindspot to set active
     * @param index Index (position in the grindspot list) of the grindspot
     */
    setActiveGrindspot(item, index) {
        this.setState({
            currentGrindspot: item,
            currentIndex: index
        });
    }

    /**
     * Updates the list of grindsposts by searching through grindspots with corresponding name
     */
    searchName() {
        GrindspotDataService.findByName(this.state.searchName)
            .then(response => {
                this.setState({
                    grindspots: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Delete a Grindspot
     * @param grindspotId Unique identifier of the Grindspot to delete
     */
    delete(grindspotId) {
        GrindspotDataService.delete(grindspotId)
            .then(response => {
                // Once we receive a successfull response we refresh the grindspots list
                this.refreshList();
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const {searchName, grindspots, currentGrindspot, currentIndex} = this.state;

        return (
            <div className="block">

                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}

                <div className={"grid-container-2"}>
                    <div className="">
                        <h4>Grindspots List</h4>
                        <div className="">
                            <div className="input-group mb-3 search-grindspot">
                                <input
                                    type="text"
                                    className="input-outlined"
                                    placeholder="Search by name"
                                    value={searchName}
                                    onChange={this.onChangeSearchName}
                                />
                                <div className="input-group-append">
                                    <button
                                        className="button-search"
                                        type="button"
                                        onClick={this.searchName}
                                    >
                                        Search
                                    </button>
                                </div>
                            </div>
                        </div>
                        <ul className="container-group-items">
                            {grindspots &&
                            grindspots.map((grindspot, index) => (
                                <li
                                    className={
                                        "list-group-item " +
                                        (index === currentIndex ? "active" : "")
                                    }
                                    onClick={() => this.setActiveGrindspot(grindspot, index)}
                                    key={index}
                                >
                                    {grindspot.name}
                                </li>
                            ))}
                        </ul>
                    </div>


                    <div>
                        <h4>Grindspot</h4>
                        {currentGrindspot ? (
                            <div>
                                <div className={"block grindspot-preview"}>
                                    <div>
                                        <label>
                                            <strong>Name :</strong>
                                        </label>
                                        {currentGrindspot.name}
                                    </div>
                                    <div>
                                        <label>
                                            <strong>Recommended AP with Tet Kutum :</strong>
                                        </label>
                                        {currentGrindspot.recommendedAP}
                                    </div>

                                    <br/>

                                    <button
                                        onClick={() => this.delete(currentGrindspot.id)}
                                        className="badge badge-danger"
                                    >
                                        Delete
                                    </button>
                                </div>
                            </div>
                        ) : (
                            <div>
                                <br/>
                                <p>Please click on a Grindspot...</p>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}
