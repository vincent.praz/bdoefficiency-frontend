import React, {Component} from "react";
import GrindspotDataService from "../services/grindspot.service";
import ItemDataService from "../services/item.service";
import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";

/**
 * Component used to create a new Grindspot
 */
export default class AddGrindspot extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeRecommendedAP = this.onChangeRecommendedAP.bind(this);
        this.checkIfInItemsList = this.checkIfInItemsList.bind(this);
        this.saveGrindspot = this.saveGrindspot.bind(this);
        this.newGrindspot = this.newGrindspot.bind(this);
        this.checkAuth = this.checkAuth.bind(this);
        this.onChangeSearchName = this.onChangeSearchName.bind(this);
        this.addItem = this.addItem.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.searchName = this.searchName.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);

        this.state = {
            id: null,
            name: "",
            recommendedAP: 0,
            items: [],

            itemsList: [],

            currentItem: null,
            currentIndex: -1,
            searchName: "",

            authorized: true,
            submitted: false,
        };
    }

    componentDidMount() {
        // Check Auth
        this.checkAuth();

        // Retrieve all items
        this.retrieveItems();
    }

    /**
     * Refresh the list
     */
    refreshList() {
        this.retrieveItems();
        this.setState({
            currentItem: null,
            currentIndex: -1
        });
    }


    /**
     * Find Item by name
     */
    searchName() {
        ItemDataService.findByName(this.state.searchName)
            .then(response => {
                this.setState({
                    itemsList: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * On Change search name refresh list
     *
     * @param e
     */
    onChangeSearchName(e) {
        const searchName = e.target.value;

        this.setState({
            searchName: searchName
        });

        // Refresh the entire list if the user deletes his search completely
        if (searchName.length === 0) {
            this.refreshList();
        }

        // Execute the search every character typed
        this.searchName();
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getAdminBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }


    /**
     * Retrieve all items in database
     */
    retrieveItems() {
        ItemDataService.getAll()
            .then(response => {
                this.setState({
                    itemsList: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeRecommendedAP(e) {
        let newValue = e.target.value;

        if (newValue < 0 || newValue > 999) {
            newValue = 0;
        }
        this.setState({
            recommendedAP: newValue
        });
    }

    /**
     * Add an item to the items selected list
     *
     * @param item The item to be added
     */
    addItem(item) {
        // We verify that the item that we want to add is not already selected
        if (!this.checkIfInItemsList(item)) {
            this.setState({
                // Adds item to the selected items list
                items: this.state.items.concat(item),
            });
        }
    }

    /**
     * Check if the item is already in the list
     * @param val The item to check
     * @returns {boolean} true if already in list | false if not
     */
    checkIfInItemsList(val) {
        return this.state.items.some(item => val.name === item.name);
    }

    /**
     * Remove Item from the selected items list
     * @param itemId Unique identifier of the item to remove
     * @param index Index (position in the list) of the item to remove
     */
    removeItem(itemId, index) {
        // Remove item from items list
        const items = this.state.items.filter(item => item.id !== itemId);
        this.setState({items: items});
    }

    /**
     * Create a new grindspot
     */
    saveGrindspot() {
        var data = {
            name: this.state.name,
            recommendedAP: this.state.recommendedAP,
            items: this.state.items,
        };

        GrindspotDataService.create(data)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    name: response.data.name,
                    recommendedAP: response.data.recommendedAP,
                    items: response.data.items,

                    submitted: true
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Resets the states if we want to create another grindspot after creating one
     */
    newGrindspot() {
        this.setState({
            id: null,
            name: "",
            recommendedAP: 0,
            items: [],

            itemsList: [],

            currentItem: null,
            currentIndex: -1,
            searchName: "",

            authorized: true,
            submitted: false,
        });
    }

    render() {
        const {searchName, itemsList, currentIndex} = this.state;
        return (
            <div className="block">
                {/* Redirect to unauthorized page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}

                {this.state.submitted ? (
                    <div className={"text-center"}>
                        <h4>You submitted successfully!</h4>
                        <button className="btn-submit-return" onClick={this.newGrindspot}>
                            Add another
                        </button>
                    </div>
                ) : (
                    <div className={"text-center"}>
                        <h4>New Grindspot</h4>
                        <div className={"input-box-add-item"}>
                            <label htmlFor="name">Name</label>
                            <input
                                type="text"
                                className="input-outlined"
                                placeholder={"Name"}
                                id="name"
                                required
                                value={this.state.name}
                                onChange={this.onChangeName}
                                name="name"
                            />
                        </div>

                        <div className={"input-box-add-item"}>
                            <label htmlFor="recommendedAP">Recommended AP with TET Kutum</label>
                            <input
                                type="number"
                                className="input-outlined"
                                id="recommendedAP"
                                min={"0"}
                                max={"999"}
                                required
                                value={this.state.recommendedAP}
                                onChange={this.onChangeRecommendedAP}
                                name="recommendedAP"
                            />
                        </div>


                        <div className={"grid-container-2"}>
                            <div>
                                <div className={""}>
                                    <h4>Items List</h4>
                                    <div className={"input-box-add-item"}>
                                        <input
                                            type="text"
                                            className="input-outlined"
                                            placeholder="Search by name"
                                            value={searchName}
                                            onChange={this.onChangeSearchName}
                                        />
                                        <div className="custom-control-inline">
                                            <button
                                                className="button-search"
                                                type="button"
                                                onClick={this.searchName}
                                            >
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="container-group-items">
                                    <ul className="text-left">
                                        {itemsList &&
                                        itemsList.map((item, index) => (
                                            <li
                                                className={
                                                    "list-group-item " +
                                                    (index === currentIndex ? "active" : "")
                                                }
                                                onClick={() => this.addItem(item, index)}
                                                key={index}
                                            >
                                                {item.icon &&
                                                <img alt="Item icon" src={"data:image/jpeg;base64," + item.icon}/>
                                                }
                                                <span>
                                                    {item.name}
                                                </span>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </div>

                            <div>
                                <h4>Items Selected</h4>
                                <div className="list-group list-group-horizontal">
                                    {this.state.items &&
                                    this.state.items.map((item, index) => (
                                        <>
                                        <span
                                            className={
                                                "list-group-item " +
                                                (index === currentIndex ? "active" : "")
                                            }
                                            onClick={() => this.removeItem(item.id, index)}
                                            key={index}
                                        >
                                            {item.icon &&
                                            <img alt="Item icon" src={"data:image/jpeg;base64," + item.icon}/>
                                            }
                                        </span>
                                        </>

                                    ))}
                                </div>
                            </div>
                        </div>

                        <button onClick={this.saveGrindspot} className={"btn-submit-item btn-submit-grindspot"}>
                            Submit
                        </button>
                    </div>
                )}
            </div>
        );
    }
}
