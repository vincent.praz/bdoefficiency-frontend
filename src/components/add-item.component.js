import React, {Component} from "react";
import ItemDataService from "../services/item.service";
import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";
import Switch from "react-switch";

/**
 * Component used to create a new Item
 */
export default class AddItem extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePrice = this.onChangePrice.bind(this);
        this.onChangeIcon = this.onChangeIcon.bind(this);
        this.onChangeMainKey = this.onChangeMainKey.bind(this);
        this.onChangeTaxed = this.onChangeTaxed.bind(this);
        this.checkAuth = this.checkAuth.bind(this);
        this.saveItem = this.saveItem.bind(this);
        this.newItem = this.newItem.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);

        this.state = {
            id: null,
            name: "",
            price: 0,
            icon: "",
            mainKey: 0,
            taxed: false,
            authorized: true,

            iconFile: "",

            submitted: false
        };

    }

    componentDidMount() {
        // Check auth
        this.checkAuth();
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getAdminBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangePrice(e) {
        let newValue = e.target.value;

        if (newValue < 0) {
            newValue = 0;
        }
        this.setState({
            price: newValue
        });
    }

    onChangeIcon(e) {
        this.setState({
            icon: e.target.value,
            iconFile: e.target.files[0]
        });
    }

    onChangeMainKey(e) {
        let newValue = e.target.value;

        if (newValue < 0) {
            newValue = 0;
        }

        this.setState({
            mainKey: newValue
        });
    }

    onChangeTaxed(e) {
        this.setState({
            taxed: !this.state.taxed
        });
    }

    /**
     * Create new item
     */
    saveItem() {
        // Building the FormData (We have to use FormData for the multipart form)
        let fd = new FormData();
        fd.append('iconFile', this.state.iconFile);
        fd.append('name', this.state.name);
        fd.append('price', this.state.price);
        fd.append('mainKey', this.state.mainKey);
        fd.append('taxed', this.state.taxed);

        ItemDataService.create(fd)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    name: response.data.name,
                    price: response.data.price,
                    icon: response.data.icon,
                    iconFile: response.data.iconFile,
                    mainKey: response.data.mainKey,
                    taxed: response.data.taxed,

                    submitted: true
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Resets states if we want to create a new item after the one just submitted
     */
    newItem() {
        this.setState({
            id: null,
            name: "",
            price: 0,
            icon: "",
            mainKey: 0,
            taxed: false,
            authorized: true,

            iconFile: "",

            submitted: false
        });
    }

    render() {
        return (
            <div className="block">
                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}

                {this.state.submitted ? (
                    <div className={"text-center"}>
                        <h4>You submitted successfully!</h4>
                        <button className="btn-submit-return" onClick={this.newItem}>
                            Add another
                        </button>
                    </div>
                ) : (
                    <div className={"text-center"}>
                        <h4>New Item</h4>
                        <div className={"input-box-add-item"}>
                            <label htmlFor="name">Name</label>
                            <input
                                type="text"
                                className="input-outlined"
                                placeholder={"Name"}
                                id="name"
                                required
                                value={this.state.name}
                                onChange={this.onChangeName}
                                name="name"
                            />
                        </div>

                        <div className={"input-box-add-item"}>
                            <label htmlFor="price">Price</label>
                            <input
                                type="number"
                                className="input-outlined"
                                id="price"
                                required
                                value={this.state.price}
                                onChange={this.onChangePrice}
                                name="price"
                            />
                        </div>

                        <div className={"input-box-add-item"}>
                            <label htmlFor="icon">Icon</label>
                            <input
                                type="file"
                                className="input-outlined input-outlined-file-picker"
                                id="icon"
                                multiple={false}
                                accept=".png,.PNG"
                                required
                                value={this.state.icon}
                                onChange={this.onChangeIcon}
                                name="icon"
                            />
                        </div>

                        <div className={"input-box-add-item"}>
                            <label htmlFor="mainKey">mainKey (BDO API ID)</label>
                            <input
                                type="number"
                                className="input-outlined"
                                id="mainKey"
                                required
                                value={this.state.mainKey}
                                onChange={this.onChangeMainKey}
                                name="mainKey"
                            />
                        </div>

                        <div className={"input-box-add-item"}>
                            <label htmlFor="taxed">Marketplace tax</label>
                            <Switch id="taxed" activeBoxShadow={"null"} offColor={"#403F51"} onColor={"#8D00F2"}
                                    onChange={this.onChangeTaxed}
                                    checked={this.state.taxed}/>
                        </div>


                        <button onClick={this.saveItem} className="btn-submit-item">
                            Submit
                        </button>
                    </div>
                )}
            </div>
        );
    }
}
