import React, {Component} from "react";
import CharacterDataService from "../services/character.service";
import AuthService from "../services/auth.service";
import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";
import ClasseDataService from "../services/classe.service";

/**
 * Component used to create a new Character
 */
export default class AddCharacter extends Component {
    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeClasse = this.onChangeClasse.bind(this);
        this.onChangeLevel = this.onChangeLevel.bind(this);
        this.onChangeAp = this.onChangeAp.bind(this);
        this.onChangeAap = this.onChangeAap.bind(this);
        this.onChangeDp = this.onChangeDp.bind(this);
        this.onChangeKutum = this.onChangeKutum.bind(this);
        this.newCharacter = this.newCharacter.bind(this);
        this.saveCharacter = this.saveCharacter.bind(this);
        this.submitCharacter = this.submitCharacter.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.retrieveClasses = this.retrieveClasses.bind(this);
        this.redirectProfile = this.redirectProfile.bind(this);
        this.getCharacter = this.getCharacter.bind(this);
        this.checkAuth = this.checkAuth.bind(this);


        this.state = {
            id: null,
            name: "",
            classe: {},
            level: 0,
            ap: 0,
            aap: 0,
            dp: 0,
            kutum: null, // 0 | 1 | 2 => No | TET | PEN
            showAlert: false,

            classesList: [],
            redirectProfile: false,


            character: {},

            authorized: true,

            submitted: false
        };
    }


    /**
     * Retrieve all grindspots
     */
    retrieveClasses() {
        ClasseDataService.getAll()
            .then(response => {
                this.setState({
                    classesList: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getUserBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeClasse(e) {
        ClasseDataService.get(e.target.value)
            .then(response => {
                this.setState({
                    classe: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }


    onChangeLevel(e) {
        if (parseInt(e.target.value) <= 999) {
            this.setState({
                level: e.target.value
            });
        }
    }

    onChangeAp(e) {
        if (parseInt(e.target.value) <= 999) {
            this.setState({
                ap: e.target.value
            });
        }
    }

    onChangeAap(e) {
        if (parseInt(e.target.value) <= 999) {
            this.setState({
                aap: e.target.value
            });
        }
    }

    onChangeDp(e) {
        if (parseInt(e.target.value) <= 999) {
            this.setState({
                dp: e.target.value
            });
        }
    }

    onChangeKutum(e) {
        if (parseInt(e.target.value) <= 999) {
            this.setState({
                kutum: e.target.value
            });
        }
    }


    /**
     * Function called to reset the states when you want to create another character after submitting the form
     */
    newCharacter() {
        this.setState({
            id: null,
            name: "",
            classe: {},
            classesList: [],
            level: 0,
            ap: 0,
            aap: 0,
            dp: 0,
            kutum: null, // 0 | 1 | 2 => No | TET | PEN

            character: {},

            authorized: true,

            submitted: false
        });

        // Retrieve all Classes
        this.retrieveClasses();
    }

    /**
     * Redirects to profile
     */
    redirectProfile() {
        this.setState({
            redirectProfile: true,
        });
    }


    /**
     * Creates a new Character
     */
    submitCharacter() {
        // We retrieve the user without its roles to avoid issues with the parsing
        let user = AuthService.getCurrentUser();
        user.roles = null;

        let data = {
            user: user,
            name: this.state.name,
            classe: this.state.classe,
            level: this.state.level,
            ap: this.state.ap,
            aap: this.state.aap,
            dp: this.state.dp,
            kutum: this.state.kutum,
        };

        CharacterDataService.create(data)
            .then(response => {
                this.setState({
                    submitted: true
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Save the character modifications
     */
    saveCharacter() {
        // We retrieve the user without its roles to avoid issues with the parsing
        let user = AuthService.getCurrentUser();
        user.roles = null;

        let data = {
            user: user,
            name: this.state.name,
            classe: this.state.classe,
            level: this.state.level,
            ap: this.state.ap,
            aap: this.state.aap,
            dp: this.state.dp,
            kutum: this.state.kutum,
        };

        CharacterDataService.update(this.state.id, data)
            .then(response => {
                this.setState({
                    submitted: true
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    componentDidMount() {
        // Scroll to top
        window.scrollTo(0, 0);

        // Check auth
        this.checkAuth();

        // Retrieve all Classes
        this.retrieveClasses();

        if (this.props.location !== undefined && this.props.location.state !== null && this.props.location.state?.reason) {
            //Show your alert here
            this.setState({
                showAlert: true,
                reason: this.props.location.state.reason
            })
        }

        if (this.props?.match?.params?.id) {
            this.getCharacter(this.props.match.params.id);
        }
    }


    /**
     * Retrieve the informations about a character
     *
     * @param characterId Unique identifier of the character to display
     */
    getCharacter(characterId) {
        CharacterDataService.get(characterId)
            .then(response => {
                this.setState({
                    character: response.data
                }, () => {
                    this.setState({
                        id: this.state.character.id,
                        name: this.state.character.name,
                        classe: this.state.character.classe,
                        level: this.state.character.level,
                        ap: this.state.character.ap,
                        aap: this.state.character.aap,
                        dp: this.state.character.dp,
                        kutum: this.state.character.kutum, // 0 | 1 | 2 => No | TET | PEN
                    });
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        return (
            <div>
                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}


                {this.state.redirectProfile && (
                    <Redirect to='/profile'/>
                )}

                {this.state.submitted ? (
                    <div className={"block block-submit"}>

                        {/* Redirect to add Grind component after character creation if we got redirected here by the addGrind component */}
                        {this.state.showAlert && (
                            <div>
                                <Redirect to={{pathname: '/addGrind'}}/>
                            </div>
                        )}


                        {/* Show the submit success if not redirected from addGrind */}
                        <h4>You submitted successfully!</h4>
                        <button className="btn-submit-return" onClick={this.newCharacter}>
                            Add another
                        </button>
                        <button className="btn-submit-return" onClick={this.redirectProfile}>
                            Back to profile
                        </button>
                    </div>
                ) : (
                    <div>
                        <header className="page-header">
                            <h1>New Character</h1>
                        </header>

                        {/* Redirect reason display */}
                        {this.state.showAlert &&
                        <div className="block" role="alert">
                            <strong>Whoops ! </strong>
                            <span>{this.state.reason}</span>
                        </div>
                        }
                        <div className={"block block-character"}>
                            <h4>Character</h4>
                            <div>
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="input-outlined"
                                    id="name"
                                    required
                                    placeholder={"Name"}
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>


                            <div>
                                <label htmlFor="classe">Class</label>
                                <select className={"input-outlined"} id="classe" name={"classe"}
                                        value={this.state.classe?.id}
                                        onChange={this.onChangeClasse}>
                                    <option disabled selected value>-- Select your Class --</option>
                                    {this.state.classesList &&
                                    this.state.classesList.map((classe, index) => (
                                        <option key={index} value={classe.id}>{classe.name}</option>
                                    ))}
                                </select>
                            </div>


                            <div>
                                <label htmlFor="kutum">Sub-Weapon</label>
                                <select className={"input-outlined"} id="kutum" name={"kutum"}
                                        value={this.state.kutum}
                                        onChange={this.onChangeKutum}>
                                    <option disabled selected value>-- Select your Offhand --</option>
                                    <option value={0}>Nouver</option>
                                    <option value={1}>TET Kutum</option>
                                    <option value={2}>PEN Kutum</option>
                                </select>
                            </div>

                            <div>
                                <label htmlFor="level">Level</label>
                                <input
                                    type="number"
                                    className="input-outlined text-center"
                                    min={"0"}
                                    max={"99"}
                                    id="level"
                                    required
                                    value={this.state.level}
                                    onChange={this.onChangeLevel}
                                    name="level"
                                />
                            </div>
                            <div className={"grid-container-3"}>
                                <div className={"col1"}>
                                    <label htmlFor="ap">AP</label>
                                    <input
                                        type="number"
                                        className="input-outlined text-center"
                                        id="ap"
                                        min={"0"}
                                        max={"999"}
                                        required
                                        value={this.state.ap}
                                        onChange={this.onChangeAp}
                                        name="ap"
                                    />
                                </div>
                                <div className={"col1"}>
                                    <label htmlFor="aap">AAP</label>
                                    <input
                                        type="number"
                                        className="input-outlined text-center"
                                        id="aap"
                                        min={"0"}
                                        max={"999"}
                                        required
                                        value={this.state.aap}
                                        onChange={this.onChangeAap}
                                        name="aap"
                                    />
                                </div>
                                <div className={"col1"}>
                                    <label htmlFor="dp">DP</label>
                                    <input
                                        type="number"
                                        className="input-outlined text-center"
                                        id="dp"
                                        min={"0"}
                                        max={"999"}
                                        required
                                        value={this.state.dp}
                                        onChange={this.onChangeDp}
                                        name="dp"
                                    />
                                </div>
                            </div>


                            {/* Display the save button when there is a character object */}
                            {this.state.character.id ? (
                                <button onClick={this.saveCharacter} className={"btn-character"}>
                                    Save
                                </button>
                            ) : (
                                <button onClick={this.submitCharacter} className={"btn-character"}>
                                    Submit
                                </button>
                            )}
                        </div>
                    </div>
                )}
            </div>
        );
    }

}
