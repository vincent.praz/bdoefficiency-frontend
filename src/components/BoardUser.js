import React, {useState, useEffect} from "react";

import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";

/**
 * User Private Component (Unused as of now except for security check)
 * @returns {JSX.Element}
 * @constructor
 */
const BoardUser = () => {
    const [error, setError] = useState("");

    useEffect(() => {
        UserService.getUserBoard().then(
            (response) => {

            },
            (error) => {
                setError(error);
            }
        );
    }, []);

    // Redirect to login page if someone tries to manipulate the url to access the page without the proper access rights
    if (error) {
        return <Redirect to='/unauthorized'/>
    }

    return (
        <div>
            <header className="jumbotron">
                <h3>User Board</h3>
            </header>
        </div>
    );
};

export default BoardUser;
