import React, {useRef, useState} from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import {isEmail} from "validator";

import AuthService from "../services/auth.service";
import BlackSpiritEfficiency from "../assets/images/icons/BlackSpiritEfficiency.png";

const required = (value) => {
    if (!value) {
        return (
            <div className="text-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

const validEmail = (value) => {
    if (!isEmail(value)) {
        return (
            <div className="text-danger" role="alert">
                This is not a valid email.
            </div>
        );
    }
};

const vusername = (value) => {
    if (value.length < 3 || value.length > 20) {
        return (
            <div className="text-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = (value) => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="text-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

/**
 * Register component
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
const Register = (props) => {
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [successful, setSuccessful] = useState(false);
    const [message, setMessage] = useState("");

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    };

    const onChangeEmail = (e) => {
        const email = e.target.value;
        setEmail(email);
    };

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };

    const handleRegister = (e) => {
        e.preventDefault();

        setMessage("");
        setSuccessful(false);

        form.current.validateAll();

        if (checkBtn.current.context._errors.length === 0) {
            AuthService.register(username, email, password).then(
                (response) => {
                    setMessage(response.data.message);
                    setSuccessful(true);
                },
                (error) => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessage(resMessage);
                    setSuccessful(false);
                }
            );
        }
    };

    return (
        <div className="block login-container">
            <img
                src={BlackSpiritEfficiency}
                alt="profile-img"
                className="profile-logo"
            />

            {message && (
                <div>
                    <div
                        className={successful ? "block text-success" : "alert alert-danger bg-danger login-alert"}
                        role="alert"
                    >
                        {message}
                    </div>
                </div>
            )}

            <Form onSubmit={handleRegister} ref={form}>
                {!successful && (
                    <div>
                        <div>
                            <label htmlFor="username">Username</label>
                            <Input
                                type="text"
                                className="input-outlined"
                                name="username"
                                value={username}
                                onChange={onChangeUsername}
                                validations={[required, vusername]}
                            />
                        </div>

                        <div>
                            <label htmlFor="email">Email</label>
                            <Input
                                type="text"
                                className="input-outlined"
                                name="email"
                                value={email}
                                onChange={onChangeEmail}
                                validations={[required, validEmail]}
                            />
                        </div>

                        <div>
                            <label htmlFor="password">Password</label>
                            <Input
                                type="password"
                                className="input-outlined"
                                name="password"
                                value={password}
                                onChange={onChangePassword}
                                validations={[required, vpassword]}
                            />
                        </div>

                        <div>
                            <button className="btn-login">Sign Up</button>
                        </div>
                    </div>
                )}

                <CheckButton style={{display: "none"}} ref={checkBtn}/>
            </Form>
        </div>
    );
};

export default Register;
