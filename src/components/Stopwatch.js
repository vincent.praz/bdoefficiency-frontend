import React, {Component} from "react";

/**
 * Stopwatch
 * Positive timer increment
 */
class Stopwatch extends Component {
    render() {
        // Formatting the results
        const timerTime = this.props.timerTime;
        let seconds = ("0" + (Math.floor(timerTime / 1000) % 60)).slice(-2);
        let minutes = ("0" + (Math.floor(timerTime / 60000) % 60)).slice(-2);
        let hours = ("0" + Math.floor(timerTime / 3600000)).slice(-2);

        return (
            <div className="Stopwatch">
                {this.props.noEdit === true ? (
                    <>
                        <div className="Stopwatch-header"><h4>Duration</h4></div>
                        <div className="Countdown-display">
                            <div className="Countdown-label">Hours : Minutes : Seconds</div>
                            <button className={"button-hidden"} onClick={() => this.props.adjustTimer("incHours")}>
                                <i className="bi bi-arrow-up"/>
                            </button>
                            <button className={"button-hidden"} onClick={() => this.props.adjustTimer("incMinutes")}>
                                <i className="bi bi-arrow-up"/>
                            </button>
                            <button className={"button-hidden"} onClick={() => this.props.adjustTimer("incSeconds")}>
                                <i className="bi bi-arrow-up"/>
                            </button>
                            <div className="Countdown-time">
                                {hours} : {minutes} : {seconds}
                            </div>
                            <button className={"button-hidden"} onClick={() => this.props.adjustTimer("decHours")}>
                                <i className="bi bi-arrow-down"/>
                            </button>
                            <button className={"button-hidden"} onClick={() => this.props.adjustTimer("decMinutes")}>
                                <i className="bi bi-arrow-down"/>
                            </button>
                            <button className={"button-hidden"} onClick={() => this.props.adjustTimer("decSeconds")}>
                                <i className="bi bi-arrow-down"/>
                            </button>
                        </div>

                        {this.props.timerOn === false && timerTime === 0 && (
                            <button className={"button-hidden"} onClick={this.props.startTimer}>Start</button>
                        )}
                        {this.props.timerOn === true && (
                            <button className={"button-hidden"} onClick={this.props.stopTimer}>Stop</button>
                        )}
                        {this.props.timerOn === false && timerTime > 0 && (
                            <button className={"button-hidden"} onClick={this.props.startTimer}>Resume</button>
                        )}
                        {this.props.timerOn === false && timerTime > 0 && (
                            <button className={"button-hidden"} onClick={this.props.resetTimer}>Reset</button>
                        )}
                    </>
                ) : (
                    <>
                        <div className="Stopwatch-header"><h4>Stopwatch</h4></div>
                        <div className="Countdown-display">
                            <div className="Countdown-label">Hours : Minutes : Seconds</div>
                            <button onClick={() => this.props.adjustTimer("incHours")}>
                                <i className="bi bi-arrow-up"/>
                            </button>
                            <button onClick={() => this.props.adjustTimer("incMinutes")}>
                                <i className="bi bi-arrow-up"/>
                            </button>
                            <button onClick={() => this.props.adjustTimer("incSeconds")}>
                                <i className="bi bi-arrow-up"/>
                            </button>
                            <div className="Countdown-time">
                                {hours} : {minutes} : {seconds}
                            </div>
                            <button onClick={() => this.props.adjustTimer("decHours")}>
                                <i className="bi bi-arrow-down"/>
                            </button>
                            <button onClick={() => this.props.adjustTimer("decMinutes")}>
                                <i className="bi bi-arrow-down"/>
                            </button>
                            <button onClick={() => this.props.adjustTimer("decSeconds")}>
                                <i className="bi bi-arrow-down"/>
                            </button>
                        </div>
                        {this.props.timerOn === false && timerTime === 0 && (
                            <button onClick={this.props.startTimer}>Start</button>
                        )}
                        {this.props.timerOn === true && (
                            <button onClick={this.props.stopTimer}>Stop</button>
                        )}
                        {this.props.timerOn === false && timerTime > 0 && (
                            <button onClick={this.props.startTimer}>Resume</button>
                        )}
                        {this.props.timerOn === false && timerTime > 0 && (
                            <button onClick={this.props.resetTimer}>Reset</button>
                        )}
                    </>
                )}


            </div>
        );
    }
}

export default Stopwatch;
