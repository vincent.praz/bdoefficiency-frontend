import React, {Component} from "react";
import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";
import ItemDataService from "../services/item.service";
import silver from "../assets/images/icons/silver.png"
import valuePack from "../assets/images/icons/value-pack.png"
import Switch from "react-switch";

/**
 * Corrupted Calculator
 * This Calculator is used to calculate how much money you can make
 * by crafting a corrupted crystal depending on the prices of the ingredients
 */
export default class CorruptedCalculator extends Component {
    constructor(props) {
        super(props);

        this.changeValuePack = this.changeValuePack.bind(this);
        this.retrieveItems = this.retrieveItems.bind(this);
        this.onChangeCorruptedPrice = this.onChangeCorruptedPrice.bind(this);
        this.onChangeForestFuryPrice = this.onChangeForestFuryPrice.bind(this);
        this.onChangeForestFuryQuantity = this.onChangeForestFuryQuantity.bind(this);
        this.onChangeCriticalHitPrice = this.onChangeCriticalHitPrice.bind(this);
        this.onChangeCriticalHitQuantity = this.onChangeCriticalHitQuantity.bind(this);
        this.onChangeMultiplier = this.onChangeMultiplier.bind(this);
        this.calculateEfficiency = this.calculateEfficiency.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.increaseForestFury = this.increaseForestFury.bind(this);
        this.decreaseForestFury = this.decreaseForestFury.bind(this);
        this.increaseCrystal = this.increaseCrystal.bind(this);
        this.decreaseCrystal = this.decreaseCrystal.bind(this);
        this.increaseMultiplier = this.increaseMultiplier.bind(this);
        this.decreaseMultiplier = this.decreaseMultiplier.bind(this);

        this.state = {
            id: null,

            // Items needed
            forestFury: {
                name: '',
                price: 0,
                icon: '',
                quantity: 0,
            },
            magicCrystalInfinity: {
                name: '',
                price: 0,
                icon: '',
                quantity: 0,
            },
            corrupted: {
                name: '',
                price: 0,
                icon: '',
                quantity: 0,
            },
            clearBlackstarCrystal: {
                name: '',
                price: 0,
                icon: '',
                quantity: 0,
            },

            valuePack: false,

            investments: 0,
            benefits: 0,
            multiplier: 1,

            authorized: true,

            submitted: false
        };
    }

    componentDidMount() {
        // Check auth
        this.checkAuth();

        // Retrieve items infos
        this.retrieveItems();
    }

    /**
     * Check if the current user has access to this page
     */
    checkAuth() {
        UserService.getUserBoard()
            .then(response => {
                this.setState({
                    authorized: true
                });
            })
            .catch(e => {
                this.setState({
                    authorized: false
                });
            });
    }

    changeValuePack() {
        this.setState({
            valuePack: !this.state.valuePack
            // Recalculate efficiency callback
        }, () => this.calculateEfficiency());
    }


    onChangeMultiplier(e) {
        let newValue = e.target.value;

        if (newValue <= 0) {
            newValue = 1;
        }
        this.setState({
            multiplier: newValue,
        });

        // Recalculate efficiency
        this.calculateEfficiency();
    }

    /**
     * This method will be called when any changes to the numbers occurs
     * it will calculate the amount of money invested and gained and determine if it is worth it or not
     */
    calculateEfficiency() {
        let investments = (this.state.forestFury.quantity * this.state.forestFury.price + this.state.magicCrystalInfinity.quantity * this.state.magicCrystalInfinity.price) * this.state.multiplier;
        let benefits = this.state.corrupted.price * this.state.multiplier;


        // Check if value pack is active
        if (this.state.valuePack) {
            benefits *= 0.845;
        } else {
            benefits *= 0.65;
        }

        this.setState({
            investments: investments,
            benefits: Math.round(benefits) - investments,
        });
    }


    onChangeCorruptedPrice(e) {
        let corrupted = this.state.corrupted;
        corrupted.price = e.target.value;

        if (e.target.value >= 0) {
            corrupted.price = e.target.value;
        } else {
            corrupted.price = 0;
        }

        this.setState({
            corrupted: corrupted
        });


        // Recalculate efficiency
        this.calculateEfficiency();
    }

    onChangeForestFuryPrice(e) {
        let forestFury = this.state.forestFury;

        if (e.target.value >= 0) {
            forestFury.price = e.target.value;
        } else {
            forestFury.price = 0;
        }

        this.setState({
            forestFury: forestFury
        });


        // Recalculate efficiency
        this.calculateEfficiency();
    }

    onChangeForestFuryQuantity(e) {
        let forestFury = this.state.forestFury;
        forestFury.quantity = e.target.value;

        this.setState({
            forestFury: forestFury
        });


        // Recalculate efficiency
        this.calculateEfficiency();
    }

    onChangeCriticalHitPrice(e) {
        let magicCrystalInfinity = this.state.magicCrystalInfinity;
        magicCrystalInfinity.price = e.target.value;

        if (e.target.value >= 0) {
            magicCrystalInfinity.price = e.target.value;
        } else {
            magicCrystalInfinity.price = 0;
        }

        this.setState({
            magicCrystalInfinity: magicCrystalInfinity
        });


        // Recalculate efficiency
        this.calculateEfficiency();
    }

    onChangeCriticalHitQuantity(e) {
        let magicCrystalInfinity = this.state.magicCrystalInfinity;
        magicCrystalInfinity.quantity = e.target.value;

        this.setState({
            magicCrystalInfinity: magicCrystalInfinity
        });


        // Recalculate efficiency
        this.calculateEfficiency();
    }

    /**
     * Increases forest fury quantity by 1
     */
    increaseForestFury() {
        let forestFury = this.state.forestFury;
        if (forestFury.quantity + 1 <= 3) {
            forestFury.quantity += 1;
            this.setState({
                forestFury: forestFury
            });

            // Recalculate efficiency
            this.calculateEfficiency();
        }
    }

    /**
     * Decreases forest fury quantity by 1
     */
    decreaseForestFury() {
        let forestFury = this.state.forestFury;
        if (forestFury.quantity - 1 >= 0) {
            forestFury.quantity -= 1;
            this.setState({
                forestFury: forestFury
            });

            // Recalculate efficiency
            this.calculateEfficiency();
        }
    }

    /**
     * Increases Crystal quantity by 1
     */
    increaseCrystal() {
        let magicCrystalInfinity = this.state.magicCrystalInfinity;
        if (magicCrystalInfinity.quantity + 1 <= 1) {
            magicCrystalInfinity.quantity += 1;
            this.setState({
                magicCrystalInfinity: magicCrystalInfinity
            });

            // Recalculate efficiency
            this.calculateEfficiency();
        }
    }

    /**
     * Decreases Crystal quantity by 1
     */
    decreaseCrystal() {
        let magicCrystalInfinity = this.state.magicCrystalInfinity;
        if (magicCrystalInfinity.quantity - 1 >= 0) {
            magicCrystalInfinity.quantity -= 1;
            this.setState({
                magicCrystalInfinity: magicCrystalInfinity
            });

            // Recalculate efficiency
            this.calculateEfficiency();
        }
    }

    /**
     * Increases Multiplies quantity by 1
     */
    increaseMultiplier() {
        if (parseInt(this.state.multiplier) + 1 <= 9999) {
            this.setState({
                multiplier: parseInt(this.state.multiplier) + 1
            });

            // Recalculate efficiency
            this.calculateEfficiency();
        }
    }

    /**
     * Decreases Multiplier quantity by 1
     */
    decreaseMultiplier() {
        if (this.state.multiplier - 1 > 0) {
            this.setState({
                multiplier: parseInt(this.state.multiplier) - 1
            });

            // Recalculate efficiency
            this.calculateEfficiency();
        }
    }


    /**
     * Retrieve infos about all the items that we need
     */
    retrieveItems() {
        ItemDataService.findByName("Forest Fury")
            .then(response => {
                // Set default values for qty
                let forestFury = response.data[0];
                forestFury.quantity = 3;

                this.setState({
                    forestFury: forestFury
                });
                ItemDataService.findByName("Magic Crystal of Infinity - Critical Hit")
                    .then(response => {
                        // Set default values for qty
                        let magicCrystalInfinity = response.data[0];
                        magicCrystalInfinity.quantity = 1;

                        this.setState({
                            magicCrystalInfinity: magicCrystalInfinity
                        });
                        ItemDataService.findByName("Corrupted Magic Crystal")
                            .then(response => {
                                this.setState({
                                    corrupted: response.data[0]
                                });

                                ItemDataService.findByName("Clear Blackstar Crystal")
                                    .then(response => {
                                        this.setState({
                                            clearBlackstarCrystal: response.data[0]
                                        });

                                        // Recalculate efficiency
                                        this.calculateEfficiency();
                                    })
                                    .catch(e => {
                                        console.log(e);
                                    });
                            })
                            .catch(e => {
                                console.log(e);
                            });
                    })
                    .catch(e => {
                        console.log(e);
                    });
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        return (
            <div>
                {/* Redirect to login page if unauthorized */}
                {!this.state.authorized && (
                    <Redirect to='/unauthorized'/>
                )}
                <div>
                    <header className="page-header">
                        <h1>Corrupted Calculator</h1>
                    </header>

                    <div className={"grid-corrupted"}>
                        <div className={"block"}>
                            <h4 className={"ingredient-title"}>{this.state.forestFury.name}</h4>

                            <div className={"corrupted-icon-container"}>
                                <div>
                                    {this.state.forestFury.icon &&
                                    <img className={"corrupted-icon"} alt="Forest fury icon"
                                         src={"data:image/jpeg;base64," + this.state.forestFury.icon}/>
                                    }

                                    <span className={"quantity-fury"}>{this.state.forestFury.quantity}</span>
                                </div>
                            </div>

                            <div className={"increase-descrease"}>
                                <button className={"button-circle button-circle--small"}
                                        onClick={this.decreaseForestFury}>
                                    <i className="bi bi-dash"/>
                                </button>

                                <button className={"button-circle button-circle--small"}
                                        onClick={this.increaseForestFury}>
                                    <i className="bi bi-plus"/>
                                </button>
                            </div>

                            <div className={"container-price"}>
                                <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                <input
                                    type="number"
                                    className="input-outlined input-outlined-price"
                                    id="price-fury"
                                    required
                                    value={this.state.forestFury.price}
                                    onChange={this.onChangeForestFuryPrice}
                                    name="price-fury"
                                />
                            </div>
                        </div>

                        <div className={"add-ingredient"}>
                            <i className="bi bi-plus"/>
                        </div>

                        <div className="block">
                            <h4 className={"ingredient-title"}>{this.state.magicCrystalInfinity.name}</h4>

                            <div className={"corrupted-icon-container"}>
                                <div>
                                    {this.state.magicCrystalInfinity.icon &&
                                    <img className={"corrupted-icon"} alt="Magic Crystal icon"
                                         src={"data:image/jpeg;base64," + this.state.magicCrystalInfinity.icon}/>
                                    }

                                    <span className={"quantity-fury"}>{this.state.magicCrystalInfinity.quantity}</span>
                                </div>
                            </div>


                            <div className={"increase-descrease"}>
                                <button className={"button-circle button-circle--small"}
                                        onClick={this.decreaseCrystal}>
                                    <i className="bi bi-dash"/>
                                </button>

                                <button className={"button-circle button-circle--small"}
                                        onClick={this.increaseCrystal}>
                                    <i className="bi bi-plus"/>
                                </button>
                            </div>


                            <div className={"container-price"}>
                                <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                <input
                                    type="number"
                                    className="input-outlined input-outlined-price"
                                    id="price-crystal"
                                    required
                                    min={"0"}
                                    value={this.state.magicCrystalInfinity.price}
                                    onChange={this.onChangeCriticalHitPrice}
                                    name="price-fury"
                                />
                            </div>
                        </div>
                        <div className={"add-ingredient"}>
                            <i className="bi bi-plus"/>
                        </div>

                        <div className="block">
                            <h4 className={"ingredient-title"}>{this.state.clearBlackstarCrystal.name}</h4>

                            <div className={"corrupted-icon-container"}>
                                <div>
                                    {this.state.clearBlackstarCrystal.icon &&
                                    <img className={"corrupted-icon"} alt="Clear Blackstar Crystal icon"
                                         src={"data:image/jpeg;base64," + this.state.clearBlackstarCrystal.icon}/>
                                    }
                                    <span className={"quantity-fury"}>1</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={"grid-corrupted"}>
                        <div className={"corrupted-block arrow-ingredients"}>
                            =
                        </div>

                        <div className={"add-ingredient"}/>

                        <div className="block corrupted-block">
                            <h4 className={"ingredient-title"}>{this.state.corrupted.name}</h4>

                            <div className={"corrupted-icon-container"}>
                                <div>
                                    {this.state.corrupted.icon &&
                                    <img className={"corrupted-icon"} alt={"Corrupted crystal icon"}
                                         src={"data:image/jpeg;base64," + this.state.corrupted.icon}/>
                                    }

                                    <span className={"quantity-fury"}>{this.state.multiplier}</span>
                                </div>
                            </div>


                            <div className={"increase-descrease"}>
                                <button className={"button-circle button-circle--small"}
                                        onClick={this.decreaseMultiplier}>
                                    <i className="bi bi-dash"/>
                                </button>

                                <button className={"button-circle button-circle--small"}
                                        onClick={this.increaseMultiplier}>
                                    <i className="bi bi-plus"/>
                                </button>
                            </div>


                            <div className={"container-price"}>
                                <img alt={"Silver icon"} className={"silver"} src={silver}/>
                                <input
                                    type="number"
                                    className="input-outlined input-outlined-price"
                                    id="price-corrupted"
                                    required
                                    min={"1"}
                                    value={this.state.corrupted.price}
                                    onChange={this.onChangeCorruptedPrice}
                                    name="price-fury"
                                />
                            </div>

                            <div className={"container-price container-multiplier"}>
                                <div className={"silver multiplier"}>
                                    <i className="bi bi-x-lg"/>
                                </div>

                                <input
                                    type="number"
                                    className="input-outlined input-outlined-price"
                                    id="multiplier"
                                    required
                                    min={"1"}
                                    max={"9999"}
                                    value={this.state.multiplier}
                                    onChange={this.onChangeMultiplier}
                                    name="multiplier"
                                />
                            </div>
                        </div>

                        <div className={"add-ingredient"}>
                            <i className="bi bi-arrow-left-right"/>
                        </div>


                        <div className={"block corrupted-block"}>
                            <h4 className={"ingredient-title"}>Results</h4>

                            <div className={"corrupted-icon-container"}>
                                <div>
                                    {this.state.valuePack ? (
                                        <img className={"corrupted-icon"} alt={"Value pack icon"}
                                             src={valuePack}/>
                                    ) : (
                                        <img className={"greyscale corrupted-icon"} alt={"Value pack icon"}
                                             src={valuePack}/>
                                    )}
                                </div>
                            </div>

                            {/* Value pack switch */}
                            <div className={"increase-descrease"}>
                                <Switch activeBoxShadow={"null"} offColor={"#403F51"} onColor={"#8D00F2"}
                                        onChange={this.changeValuePack}
                                        checked={this.state.valuePack}/>
                            </div>

                            <div className={"invest-benef"}>
                                <div className={"flex-box-2"}>
                                    <div className={"col1"}>INVESTMENTS</div>
                                    <div className={"col1"}>{this.state.investments.toLocaleString()}</div>
                                </div>

                                {this.state.benefits >= 0 ? (
                                    <div className={"flex-box-2"}>
                                        <div className={"col1"}>BENEFITS</div>
                                        <div
                                            className={"col1 text-success"}>{this.state.benefits.toLocaleString()}</div>
                                    </div>
                                ) : (

                                    <div className={"flex-box-2"}>
                                        <div className={"col1"}>LOSSES</div>
                                        <div className={"col1 text-danger"}>{this.state.benefits.toLocaleString()}</div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
