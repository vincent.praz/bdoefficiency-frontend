import React, {useState, useEffect} from "react";
import {NavLink, Redirect, BrowserRouter, Route, Switch} from 'react-router-dom'

import UserService from "../services/user.service";
import AuthService from "../services/auth.service";
import AddItem from "./add-item.component";
import AddGrindspot from "./add-grindspot.component";
import ItemsList from "./items-list.component";
import GrindspotsList from "./grindspots-list.component";
import GrindsList from "./grinds-list.component";
import Home from "./Home";
import Statistics from "./Statistics";
import Login from "./Login";
import Unauthorized from "./Unauthorized";
import Register from "./Register";
import Profile from "./Profile";
import BoardModerator from "./BoardModerator";
import AddCharacter from "./add-character.component";
import CorruptedCalculator from "./CorruptedCalculator.component";
import ScrollCalculator from "./ScrollCalculator.component";
import AddGrind from "./add-grind.component";
import Brackets from "./Brackets";
import CaphrasCalculator from "./CaphrasCalculator.component";
import Tax from "./tax.component";
import Grind from "./grind.component";

/**
 * Admin private component
 * @returns {JSX.Element}
 * @constructor
 */
const BoardUser = () => {
    const [error, setError] = useState("");
    const [showAdminBoard, setShowAdminBoard] = useState(false);

    useEffect(() => {
        UserService.getAdminBoard().then(
            (response) => {
                const user = AuthService.getCurrentUser();
                setShowAdminBoard(user.roles.includes("ROLE_ADMIN"));
            },
            (error) => {
                setError(error);
            }
        );
    }, []);

    // Redirect to login page if someone tries to manipulate the url to access the page without the proper access rights
    if (error) {
        return <Redirect to='/unauthorized'/>
    }

    return (
        <div>

            <header className="page-header">
                <h1>Admin Board</h1>
            </header>

            <div className={"page-admin"}>
                <BrowserRouter>
                    <Switch>
                        <Route>
                            {showAdminBoard && (
                                <div className={"block"}>
                                    <nav className="navbar navbar-expand navbar-admin">
                                        <button className="nav-item">
                                            <NavLink to={"/addItem"} className="nav-link">
                                                Add Item
                                            </NavLink>
                                        </button>
                                        <button className="nav-item">
                                            <NavLink to={"/addGrindspot"} className="nav-link">
                                                Add Grindspot
                                            </NavLink>
                                        </button>
                                        <button className="nav-item">
                                            <NavLink to={"/items"} className="nav-link">
                                                Items List
                                            </NavLink>
                                        </button>
                                        <button className="nav-item">
                                            <NavLink to={"/grindspots"} className="nav-link">
                                                Grindspots List
                                            </NavLink>
                                        </button>

                                        <button className="nav-item">
                                            <NavLink to={"/grinds-admin"} className="nav-link">
                                                Grind sessions List
                                            </NavLink>
                                        </button>
                                    </nav>
                                </div>
                            )}


                            {/* Routes */}
                            <div>
                                <Route exact path={["/", "/home"]} component={Home}/>
                                <Route exact path="/statistics" component={Statistics}/>
                                <Route exact path="/login" component={Login}/>
                                <Route exact path="/unauthorized" component={Unauthorized}/>
                                <Route exact path="/register" component={Register}/>
                                <Route exact path="/profile" component={Profile}/>
                                <Route path="/user" component={BoardUser}/>
                                <Route path="/mod" component={BoardModerator}/>
                                <Route exact path="/addItem" component={AddItem}/>
                                <Route exact path="/addGrindspot" component={AddGrindspot}/>
                                <Route exact path={["/addCharacter", "/characters/edit/:id"]} component={AddCharacter}/>
                                <Route exact path="/corrupted" component={CorruptedCalculator}/>
                                <Route exact path="/scroll" component={ScrollCalculator}/>
                                <Route exact path={["/addGrind", "/grinds/edit/:id"]} component={AddGrind}/>
                                <Route exact path="/items" component={ItemsList}/>
                                <Route exact path="/brackets" component={Brackets}/>
                                <Route exact path="/caphras" component={CaphrasCalculator}/>
                                <Route exact path="/tax" component={Tax}/>
                                <Route path="/grinds/show/:id" component={Grind}/>
                                <Route exact path="/grindspots" component={GrindspotsList}/>
                                <Route exact path="/grinds-admin" component={GrindsList}/>
                            </div>
                        </Route>
                    </Switch>
                </BrowserRouter>


            </div>

        </div>
    );
};

export default BoardUser;
