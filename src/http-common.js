import axios from "axios";

export default axios.create({
    // TODO: Change url for the backend in prod
    baseURL: "http://localhost:8080/api",
    headers: {
        "Content-type": "application/json"
    }
});
