import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";

import './styles.css';
import {BrowserRouter, NavLink, Route, Switch} from "react-router-dom";
import AddItem from "./components/add-item.component";
import ItemsList from "./components/items-list.component";
import AddGrindspot from "./components/add-grindspot.component";
import AddCharacter from "./components/add-character.component";
import CorruptedCalculator from "./components/CorruptedCalculator.component";
import ScrollCalculator from "./components/ScrollCalculator.component";
import AddGrind from "./components/add-grind.component";
import GrindspotsList from "./components/grindspots-list.component";
import Brackets from "./components/Brackets";
import CaphrasCalculator from "./components/CaphrasCalculator.component";
import Tax from "./components/tax.component";
import Statistics from "./components/Statistics";
import Grind from "./components/grind.component";
import Login from "./components/Login";
import Unauthorized from "./components/Unauthorized";
import Register from "./components/Register";
import Home from "./components/Home";
import Profile from "./components/Profile";
import BoardUser from "./components/BoardUser";
import BoardModerator from "./components/BoardModerator";
import BoardAdmin from "./components/BoardAdmin";
import GrindsList from "./components/grinds-list.component";
import PrivacyPolicies from "./components/PrivacyPolicies"
import Navbar from './components/Nav/Navbar';

function App() {

    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route>
                        <Navbar/>
                        <div className="wrapper">
                            <Route exact path={["/", "/home"]} component={Home}/>
                            <Route exact path="/statistics" component={Statistics}/>
                            <Route exact path="/login" component={Login}/>
                            <Route exact path="/unauthorized" component={Unauthorized}/>
                            <Route exact path="/register" component={Register}/>
                            <Route exact path="/profile" component={Profile}/>
                            <Route path="/user" component={BoardUser}/>
                            <Route path="/mod" component={BoardModerator}/>
                            <Route path="/admin" component={BoardAdmin}/>
                            <Route exact path="/addItem" component={AddItem}/>
                            <Route exact path="/addGrindspot" component={AddGrindspot}/>
                            <Route exact path={["/addCharacter", "/characters/edit/:id"]} component={AddCharacter}/>
                            <Route exact path="/corrupted" component={CorruptedCalculator}/>
                            <Route exact path="/scroll" component={ScrollCalculator}/>
                            <Route exact path={["/addGrind", "/grinds/edit/:id"]} component={AddGrind}/>
                            <Route exact path="/items" component={ItemsList}/>
                            <Route exact path="/grindspots" component={GrindspotsList}/>
                            <Route exact path="/brackets" component={Brackets}/>
                            <Route exact path="/caphras" component={CaphrasCalculator}/>
                            <Route exact path="/tax" component={Tax}/>
                            <Route path="/grinds/show/:id" component={Grind}/>
                            <Route exact path="/grinds-admin" component={GrindsList}/>
                            <Route exact path="/privacy" component={PrivacyPolicies}/>
                        </div>
                    </Route>
                </Switch>

                <footer>
                    <div className={"grid-container-3"}>
                        <div className={"col1"}>
                            <NavLink to={"/privacy"}>
                                Terms and Privacy Policies
                            </NavLink>
                        </div>
                        <div className="col1 text-center">
                            © Copyright Vincent Praz 2021
                        </div>
                        <div className="col1 text-right">
                            <div className={"grid-container-4"}>
                                <div className={"col1"}>
                                    <a href={"mailto:bdoefficiency@gmail.com"} className={"envelope"}>
                                        <i className="bi bi-envelope-fill"/>
                                        <i className="bi bi-envelope-open-fill"/>
                                    </a>
                                </div>
                                <div className={"col1"}>
                                    <a href={"https://www.linkedin.com/in/vincent-praz/"} className={"envelope"}>
                                        <i className="bi bi-linkedin"/>
                                    </a>
                                </div>
                                <div className={"col1"}>
                                    <a href={"https://github.com/zelyvanna"} className={"envelope"}>
                                        <i className="bi bi-github"/>
                                    </a>
                                </div>
                                <div className={"col1"}>
                                    <a href={"https://gitlab.com/vincent.praz"} className={"envelope"}>
                                        <i className="fab fa-gitlab"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </BrowserRouter>


        </div>
    );
}

export default App;
