import http from "../http-common";
import authHeader from "./auth-header";

/**
 * Classe Service
 */
class ClasseDataService {

    /**
     * Retrieve all Classes
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAll() {
        return http.get("/classes", {headers: authHeader()});
    }

    /**
     * Get class - character population
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAllCount() {
        return http.get("/classes/count", {headers: authHeader()});
    }

    /**
     * Retrieve informations about a specific Classe by its unique identifier
     *
     * @param id Unique identifier of the Classe
     * @returns {Promise<AxiosResponse<any>>}
     */
    get(id) {
        return http.get(`/classes/${id}`, {headers: authHeader()});
    }

    /**
     * Create a new Classe
     *
     * @param data
     * @returns {Promise<AxiosResponse<any>>}
     */
    create(data) {
        /**
         * Creates a new classe
         *
         * @param data
         * @returns {Promise<AxiosResponse<any>>}
         */
        return http.post("/classes", data, {
            headers: authHeader(),
        });
    }

    /**
     * Delete a grind hour by it's id
     *
     * @param id the unique identifier of the grind hour to be deleted
     * @returns {Promise<AxiosResponse<any>>}
     */
    delete(id) {
        return http.delete(`/classes/${id}`, {headers: authHeader()});
    }
}

export default new ClasseDataService();
