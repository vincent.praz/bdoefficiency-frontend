import http from "../http-common";
import authHeaderMultipart from "./auth-header-multipart";
import authHeader from "./auth-header";

/**
 * Item Service
 */
class ItemDataService {

    /**
     * Retrieve all items
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAll() {
        return http.get("/items", {headers: authHeader()});
    }

    /**
     * Retrieve item by its unique identifier
     *
     * @param id Unique identifier of the item
     * @returns {Promise<AxiosResponse<any>>}
     */
    get(id) {
        return http.get(`/items/${id}`);
    }

    /**
     * Creates a new item
     *
     * @param data
     * @returns {Promise<AxiosResponse<any>>}
     */
    create(data) {
        return http.post("/items", data, {
            headers: authHeaderMultipart(),
        });
    }

    /**
     * Search the items by their names
     *
     * @param name name of the items we want to search for
     * @returns {Promise<AxiosResponse<any>>}
     */
    findByName(name) {
        return http.get(`/items?name=${name}`, {headers: authHeader()});
    }

    /**
     * Recalculates the item marketplace prices
     *
     * @param id The id of the item we want to recalculate
     * @returns {Promise<AxiosResponse<any>>}
     */
    recalculatePrice(id) {
        return http.patch(`/items/${id}/recalculate`, null, {headers: authHeader()});
    }


    /**
     * Recalculates the item marketplace prices
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    recalculateAllPrices() {
        return http.patch(`/items/0/recalculate`, null, {headers: authHeader()});
    }

    /**
     * Delete an item by it's id
     *
     * @param id the unique identifier of the item to be deleted
     * @returns {Promise<AxiosResponse<any>>}
     */
    delete(id) {
        return http.delete(`/items/${id}`, {headers: authHeader()});
    }
}

export default new ItemDataService();
