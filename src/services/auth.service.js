import axios from "axios";

const API_URL = "http://localhost:8080/api/auth/";

const register = (username, email, password) => {
    return axios.post(API_URL + "signup", {
        username,
        email,
        password,
    });
};

/**
 * Logs an user in
 *
 * @param username Username of the user to log in
 * @param password Password of the user to log in
 * @returns {Promise<AxiosResponse<any>>}
 */
const login = (username, password) => {
    return axios
        .post(API_URL + "signin", {
            username,
            password,
        })
        .then((response) => {
            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }

            return response.data;
        });
};

/**
 * Logs an user out
 */
const logout = () => {
    localStorage.removeItem("user");
};

/**
 * Retrieves current user
 *
 * @returns {any}
 */
const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
};

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {
    register,
    login,
    logout,
    getCurrentUser,
};
