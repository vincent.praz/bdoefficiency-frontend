import http from "../http-common";
import authHeader from "./auth-header";

/**
 * Character Service
 */
class CharacterDataService {

    /**
     * Retrieve all characters
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAll() {
        return http.get("/characters", {headers: authHeader()});
    }

    /**
     * Search the Characters by their owner
     *
     * @param user owner of the characters
     * @returns {Promise<AxiosResponse<any>>}
     */
    getByUser(username) {
        return http.get(`/characters?username=${username}`, {headers: authHeader()});
    }

    /**
     * Retrieve a Character by it's unique identifier
     *
     * @param id Unique identifier of the Character
     * @returns {Promise<AxiosResponse<any>>}
     */
    get(id) {
        return http.get(`/characters/${id}`, {headers: authHeader()});
    }

    /**
     * Creates a new character
     *
     * @param data
     * @returns {Promise<AxiosResponse<any>>}
     */
    create(data) {
        return http.post("/characters", data, {
            headers: authHeader(),
        });
    }

    /**
     * Search the grindspots by their names
     *
     * @param name name of the grindspots we want to search for
     * @returns {Promise<AxiosResponse<any>>}
     */
    findByName(name) {
        return http.get(`/characters?name=${name}`, {headers: authHeader()});
    }

    /**
     * Search the characters by their ids
     *
     * @param id id of the characters we want to search for
     * @returns {Promise<AxiosResponse<any>>}
     */
    findById(id) {
        return http.get(`/characters/${id}`, {headers: authHeader()});
    }


    /**
     * Update the Character
     *
     * @param id Unique character identifier
     * @param data datas to update
     * @returns {Promise<AxiosResponse<any>>}
     */
    update(id, data) {
        return http.put(`/characters/${id}`, data, {headers: authHeader()});
    }

    /**
     * Delete a character by it's id
     *
     * @param id the unique identifier of the character to be deleted
     * @returns {Promise<AxiosResponse<any>>}
     */
    delete(id) {
        return http.delete(`/characters/${id}`, {headers: authHeader()});
    }
}

export default new CharacterDataService();
