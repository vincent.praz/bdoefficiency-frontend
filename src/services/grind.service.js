import http from "../http-common";
import authHeader from "./auth-header";

/**
 * Grind session Service
 */
class GrindDataService {

    /**
     * Retrieve all Grind sessions
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAll() {
        return http.get("/grinds", {headers: authHeader()});
    }

    /**
     * Retrieve a limited number of Grind sessions
     * @param limit True if we want a limited number
     * @returns {Promise<AxiosResponse<any>>}
     */
    getLimitedNumberOfGrinds(limit) {
        return http.get(`/grinds?limit=${limit}`, {headers: authHeader()});
    }

    /**
     * Get grind session for a specific user
     *
     * @param username username of the specific user
     * @returns {Promise<AxiosResponse<any>>}
     */
    getByUser(username) {
        return http.get(`/grinds?username=${username}`, {headers: authHeader()});
    }

    /**
     * Retrieve infos about average money per hour per grindspot for the authenticated user
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAveragePerGrindspot() {
        return http.get("/grinds/average_per_user", {headers: authHeader()});
    }

    /**
     * Get informations about average money per hour per class
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAveragePerHourByClass() {
        return http.get("/grinds/per_class", {headers: authHeader()});
    }

    /**
     * Retrieve a specific Grind session by its unique identifier
     *
     * @param id Unique identifier of the grind session
     * @returns {Promise<AxiosResponse<any>>}
     */
    get(id) {
        return http.get(`/grinds/${id}`, {headers: authHeader()});
    }

    /**
     * Creates a new grind hour
     *
     * @param data
     * @returns {Promise<AxiosResponse<any>>}
     */
    create(data) {
        return http.post("/grinds", data, {
            headers: authHeader(),
        });
    }

    // Update grind hour
    update(id, data) {
        return http.put(`/grinds/${id}`, data, {headers: authHeader()});
    }

    /**
     * Delete a grind hour by it's id
     *
     * @param id the unique identifier of the grind hour to be deleted
     * @returns {Promise<AxiosResponse<any>>}
     */
    delete(id) {
        return http.delete(`/grinds/${id}`, {headers: authHeader()});
    }
}

export default new GrindDataService();
