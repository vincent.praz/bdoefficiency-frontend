import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:8080/api/test/";

/**
 * Get public board
 *
 * @returns {Promise<AxiosResponse<any>>}
 */
const getPublicContent = () => {
    return axios.get(API_URL + "all");
};

/**
 * Get user board
 *
 * @returns {Promise<AxiosResponse<any>>}
 */
const getUserBoard = () => {
    return axios.get(API_URL + "user", {headers: authHeader()});
};

/**
 * Get moderator board
 *
 * @returns {Promise<AxiosResponse<any>>}
 */
const getModeratorBoard = () => {
    return axios.get(API_URL + "mod", {headers: authHeader()});
};

/**
 * Get admin board
 *
 * @returns {Promise<AxiosResponse<any>>}
 */
const getAdminBoard = () => {
    return axios.get(API_URL + "admin", {headers: authHeader()});
};

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {
    getPublicContent,
    getUserBoard,
    getModeratorBoard,
    getAdminBoard,
};
