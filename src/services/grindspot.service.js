import http from "../http-common";
import authHeader from "./auth-header";

/**
 * Grindspot Service
 */
class GrindspotDataService {

    /**
     * Retrieve all Grindspots
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAll() {
        return http.get("/grindspots", {headers: authHeader()});
    }

    /**
     * Get grindspots - Number of grind sessions on the grindspot
     *
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAllCount() {
        return http.get("/grindspots/count", {headers: authHeader()});
    }

    /**
     * Retrieve Grindspot by its unique identifier
     *
     * @param id Unique identifier of the Grindspot
     * @returns {Promise<AxiosResponse<any>>}
     */
    get(id) {
        return http.get(`/grindspots/${id}`);
    }

    /**
     * Creates a new grindspot
     *
     * @param data
     * @returns {Promise<AxiosResponse<any>>}
     */
    create(data) {
        return http.post("/grindspots", data, {
            headers: authHeader(),
        });
    }

    /**
     * Search the grindspots by their names
     *
     * @param name name of the grindspots we want to search for
     * @returns {Promise<AxiosResponse<any>>}
     */
    findByName(name) {
        return http.get(`/grindspots?name=${name}`, {headers: authHeader()});
    }


    /**
     * Search the grindspots by their ids
     *
     * @param id id of the grindspots we want to search for
     * @returns {Promise<AxiosResponse<any>>}
     */
    findById(id) {
        return http.get(`/grindspots/${id}`, {headers: authHeader()});
    }

    /**
     * Delete a grindspot by it's id
     *
     * @param id the unique identifier of the grindspot to be deleted
     * @returns {Promise<AxiosResponse<any>>}
     */
    delete(id) {
        return http.delete(`/grindspots/${id}`, {headers: authHeader()});
    }
}

export default new GrindspotDataService();
