// Checks Local Storage for user item.
export default function authHeader() {
    const user = JSON.parse(localStorage.getItem('user'));

    // If there is a logged in user with accessToken (JWT)
    if (user && user.accessToken) {
        // return HTTP Authorization header
        return {Authorization: 'Bearer ' + user.accessToken};
    } else {
        // Otherwise, return an empty object
        return {};
    }
}
