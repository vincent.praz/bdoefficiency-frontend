/**
 * This file contains the infos about the caphras levels enhancements of the Green armor
 *
 * We use constants because these values are not changing
 */

/**
 * Infos about Caphras enhancement Level 0
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL0 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 0,
};

/**
 * Infos about Caphras enhancement Level 1
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL1 = {
    AP: 0,
    DP: 1,
    Evasion: 1,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 1,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 28,
};

/**
 * Infos about Caphras enhancement Level 2
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL2 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 1,
    HP: 0,
    StonesQuantity: 43,
};

/**
 * Infos about Caphras enhancement Level 3
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL3 = {
    AP: 0,
    DP: 1,
    Evasion: 0,
    DR: 1,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 57,
};

/**
 * Infos about Caphras enhancement Level 4
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL4 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 20,
    StonesQuantity: 57,
};

/**
 * Infos about Caphras enhancement Level 5
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL5 = {
    AP: 0,
    DP: 1,
    Evasion: 1,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 1,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 134,
};

/**
 * Infos about Caphras enhancement Level 6
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL6 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 1,
    HP: 0,
    StonesQuantity: 134,
};

/**
 * Infos about Caphras enhancement Level 7
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL7 = {
    AP: 0,
    DP: 1,
    Evasion: 0,
    DR: 1,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 206,
};

/**
 * Infos about Caphras enhancement Level 8
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL8 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 20,
    StonesQuantity: 224,
};

/**
 * Infos about Caphras enhancement Level 9
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL9 = {
    AP: 0,
    DP: 1,
    Evasion: 1,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 1,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 264,
};

/**
 * Infos about Caphras enhancement Level 10
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL10 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 1,
    HP: 0,
    StonesQuantity: 305,
};

/**
 * Infos about Caphras enhancement Level 11
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL11 = {
    AP: 0,
    DP: 1,
    Evasion: 0,
    DR: 1,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 336,
};

/**
 * Infos about Caphras enhancement Level 12
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL12 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 20,
    StonesQuantity: 376,
};

/**
 * Infos about Caphras enhancement Level 13
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL13 = {
    AP: 0,
    DP: 1,
    Evasion: 1,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 1,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 416,
};

/**
 * Infos about Caphras enhancement Level 14
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL14 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 1,
    HP: 0,
    StonesQuantity: 456,
};

/**
 * Infos about Caphras enhancement Level 15
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL15 = {
    AP: 0,
    DP: 1,
    Evasion: 0,
    DR: 1,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 496,
};

/**
 * Infos about Caphras enhancement Level 16
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL16 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 20,
    StonesQuantity: 536,
};

/**
 * Infos about Caphras enhancement Level 17
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL17 = {
    AP: 0,
    DP: 1,
    Evasion: 1,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 1,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 576,
};

/**
 * Infos about Caphras enhancement Level 18
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL18 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 1,
    HP: 0,
    StonesQuantity: 616,
};

/**
 * Infos about Caphras enhancement Level 19
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL19 = {
    AP: 0,
    DP: 1,
    Evasion: 0,
    DR: 1,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 656,
};

/**
 * Infos about Caphras enhancement Level 20
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL20 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 20,
    StonesQuantity: 696,
};
