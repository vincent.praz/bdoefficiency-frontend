/**
 * This file contains the infos about the caphras levels enhancements of the Boss Awakening Weapon
 * We use constants because these values are not changing
 */



/**
 * Infos about Caphras enhancement Level 0
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL0 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 0,
};

/**
 * Infos about Caphras enhancement Level 1
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL1 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 345,
};

/**
 * Infos about Caphras enhancement Level 2
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL2 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 3,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 451,
};

/**
 * Infos about Caphras enhancement Level 3
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL3 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 556,
};

/**
 * Infos about Caphras enhancement Level 4
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL4 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 662,
};

/**
 * Infos about Caphras enhancement Level 5
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL5 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 1044,
};

/**
 * Infos about Caphras enhancement Level 6
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL6 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 1176,
};

/**
 * Infos about Caphras enhancement Level 7
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL7 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 1308,
};

/**
 * Infos about Caphras enhancement Level 8
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL8 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 1440,
};

/**
 * Infos about Caphras enhancement Level 9
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL9 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 1573,
};

/**
 * Infos about Caphras enhancement Level 10
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL10 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 1705,
};

/**
 * Infos about Caphras enhancement Level 11
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL11 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 1752,
};

/**
 * Infos about Caphras enhancement Level 12
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL12 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 1884,
};

/**
 * Infos about Caphras enhancement Level 13
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL13 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 2016,
};

/**
 * Infos about Caphras enhancement Level 14
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL14 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 2148,
};

/**
 * Infos about Caphras enhancement Level 15
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL15 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 2280,
};

/**
 * Infos about Caphras enhancement Level 16
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL16 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 2412,
};

/**
 * Infos about Caphras enhancement Level 17
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL17 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 2544,
};

/**
 * Infos about Caphras enhancement Level 18
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL18 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 2676,
};

/**
 * Infos about Caphras enhancement Level 19
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL19 = {
    AP: 1,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 0,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 2808,
};

/**
 * Infos about Caphras enhancement Level 20
 *
 * @type {{Accuracy: number, Evasion: number, StonesQuantity: number, HiddenDamageReduction: number, HP: number, DP: number, DR: number, AP: number, HiddenEvasion: number}}
 */
export const LEVEL20 = {
    AP: 0,
    DP: 0,
    Evasion: 0,
    DR: 0,
    Accuracy: 1,
    HiddenEvasion: 0,
    HiddenDamageReduction: 0,
    HP: 0,
    StonesQuantity: 2988,
};
